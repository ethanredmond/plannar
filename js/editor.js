// Built in Editor.inc:
// let objectHash         = '';
// const blocksArr        = {};
// let blocksHotbarArr    = [];
// let selectedBlockIndex = 0;

let selectedBlockId = blocksHotbarArr[selectedBlockIndex];

let columnsNum;
let rowsNum;

let $objectName;
let $columnsNum;
let $rowsNum;
let $planContainer;
let $planTable;
let $blocksHotbar;
let $duplicatePlanModal;
let $inventoryModal;
let sidePanel;

let rightClickToPlace = false;

let $mouseOveredBlock = null;

$(document).ready(function () {
    const inventoryModal = M.Modal.getInstance(byId('inventoryModal'));
    sidePanel = M.Sidenav.getInstance(byId('slide-out'));

    rightClickToPlace = JSON.parse(localStorage.getItem('rightClickToPlace'));
    $('#rightClickToPlace').prop('checked', rightClickToPlace).on('change', function () {
        rightClickToPlace = $(this).is(':checked');
        localStorage.setItem('rightClickToPlace', JSON.stringify(rightClickToPlace));
    });


    // Change number input on scroll.
    $('#columnsNum, #rowsNum').hover(
        function () {
            $(this).on('mousewheel DOMMouseScroll', function (event) {
                const $curElem = $(event.target);
                const currVal = parseInt($curElem.val());
                if (event.originalEvent.wheelDelta > 0) {
                    if (currVal + 1 <= $curElem.attr('max')) $curElem.val(currVal + 1);
                } else {
                    if (currVal - 1 >= $curElem.attr('min')) $curElem.val(currVal - 1);
                }
                $curElem.trigger('input');
            });
        },
        function () {
            $(this).off('mousewheel DOMMouseScroll');
        }
    ).on('input', function () {
        $(window).trigger('resize');
    });

    $objectName = $('#objectName');
    $columnsNum = $('#columnsNum');
    $rowsNum = $('#rowsNum');
    $planContainer = $('#planContainer');
    $planTable = $('#planTable');
    $blocksHotbar = $('#blocksHotbar');
    $inventoryModal = $('#inventoryModal');
    $duplicatePlanModal = $('#duplicatePlanModal');
    columnsNum = $columnsNum.val();
    rowsNum = $rowsNum.val();

    // https://codepen.io/Momciloo/pen/bpyMbB
    $.fn.textWidth = function (text, font) {
        if (!$.fn.textWidth.fakeEl) $.fn.textWidth.fakeEl = $('<span>').hide().appendTo(document.body);
        $.fn.textWidth.fakeEl.text(text || this.val() || this.text() || this.attr('placeholder')).css('font', font || this.css('font'));
        return $.fn.textWidth.fakeEl.width();
    };
    $objectName.on('input', function () {
        const $this = $(this);
        $this.css({
            width: $this.textWidth()
        });
        const val = $this.val();
        if (val === '') {
            document.title = "Untitled plan - Plannar";
        } else {
            document.title = val + " - Plannar";
        }
    }).trigger('input');

    // Add columns and rows on change.
    $columnsNum.on('input', function () {
        const max = parseInt(this.max);
        if (parseInt(this.value) > max) {
            this.value = max;
        }
        const min = parseInt(this.min);
        if (parseInt(this.value) < min) {
            this.value = min;
        }

        if (this.value != null && this.value !== '') {
            const columnsNumPrev = columnsNum;
            columnsNum = parseInt(this.value);
            // console.log('columnsNum:', columnsNum);
            // console.log('columnsNumPrev:', columnsNumPrev);
            if (columnsNum > columnsNumPrev) {
                let tdHtml = '';
                for (let columnNum = columnsNumPrev + 1; columnNum <= columnsNum; columnNum++) {
                    const rowNum = $(this).data('row');
                    tdHtml += "<td class=\"planRow" + rowNum + " planColumn" + columnNum + "\" data-row=\"" + rowNum + "\" data-column=\"" + columnNum + "\"></td>";
                }
                $('#planTable tr').append(tdHtml);
            } else if (columnsNum < columnsNumPrev) {
                for (let columnNum = columnsNum + 1; columnNum <= columnsNumPrev; columnNum++) {
                    $('#planTable tr td.planColumn' + columnNum).remove();
                }
            }
        }
    });
    $rowsNum.on('input', function () {
        const max = parseInt(this.max);
        if (parseInt(this.value) > max) {
            this.value = max;
        }
        const min = parseInt(this.min);
        if (parseInt(this.value) < min) {
            this.value = min;
        }

        if (this.value != null && this.value !== '') {
            const rowsNumPrev = rowsNum;
            rowsNum = parseInt(this.value);
            // console.log('rowsNum:', rowsNum);
            // console.log('rowsNumPrev:', rowsNumPrev);
            if (rowsNum > rowsNumPrev) {
                let trHtml = '';
                for (let rowNum = rowsNumPrev + 1; rowNum <= rowsNum; rowNum++) {
                    trHtml += "<tr class=\"planRow" + rowNum + "\" data-row=\"" + rowNum + "\">";
                    for (let columnNum = 1; columnNum <= columnsNum; columnNum++) {
                        trHtml += "<td class=\"planRow" + rowNum + " planColumn" + columnNum + "\" data-row=\"" + rowNum + "\" data-column=\"" + columnNum + "\"></td>";
                    }
                    trHtml += "</tr>";
                }
                $('#planTable').append(trHtml);
            } else if (rowsNum < rowsNumPrev) {
                for (let rowNum = rowsNum + 1; rowNum <= rowsNumPrev; rowNum++) {
                    $('#planTable tr.planRow' + rowNum).remove();
                }
            }
        }
    });

    $(document.body).on('mousedown', '#planTable tr td', function (event) {
        isClickdown = true;
        const $this = $(this);
        handleMouseEvent($this, event);
        // console.log('isClickdown:', isClickdown);
        event.preventDefault();
    });

    $(document.body).on('mouseover', '#planTable tr td', function (event) {
        if (isClickdown) {
            const $this = $(this);
            handleMouseEvent($this, event);
            event.preventDefault();
        }
    });

    $(document.body).on('mouseup', '#planTable tr td', function () {
        isClickdown = false;
        // console.log('isClickdown:', isClickdown);
        event.preventDefault();
    });

    function keyboardSelectBlock (index) {
        blocksHotbarArr[index] = $mouseOveredBlock.data('block-id');
        initBlocksHotbar();
    }

    $(document).keydown(function (event) {
        if ($(event.target).closest('input')[0]) {
            return;
        }
        if (!event.metaKey && !event.ctrlKey && !event.shiftKey && !event.altKey) {
            if ($mouseOveredBlock != null) {
                // @formatter:off
                if (event.which === 49 || event.which ===  97) { keyboardSelectBlock(0); return false; } // 1 was pressed.
                if (event.which === 50 || event.which ===  98) { keyboardSelectBlock(1); return false; } // 2 was pressed.
                if (event.which === 51 || event.which ===  99) { keyboardSelectBlock(2); return false; } // 3 was pressed.
                if (event.which === 52 || event.which === 100) { keyboardSelectBlock(3); return false; } // 4 was pressed.
                if (event.which === 53 || event.which === 101) { keyboardSelectBlock(4); return false; } // 5 was pressed.
                if (event.which === 54 || event.which === 102) { keyboardSelectBlock(5); return false; } // 6 was pressed.
                if (event.which === 55 || event.which === 103) { keyboardSelectBlock(6); return false; } // 7 was pressed.
                if (event.which === 56 || event.which === 104) { keyboardSelectBlock(7); return false; } // 8 was pressed.
                if (event.which === 57 || event.which === 105) { keyboardSelectBlock(8); return false; } // 9 was pressed.
                // @formatter:on
            } else {
                // @formatter:off
                if (event.which === 49 || event.which ===  97) { selectBlock(0); return false; } // 1 was pressed.
                if (event.which === 50 || event.which ===  98) { selectBlock(1); return false; } // 2 was pressed.
                if (event.which === 51 || event.which ===  99) { selectBlock(2); return false; } // 3 was pressed.
                if (event.which === 52 || event.which === 100) { selectBlock(3); return false; } // 4 was pressed.
                if (event.which === 53 || event.which === 101) { selectBlock(4); return false; } // 5 was pressed.
                if (event.which === 54 || event.which === 102) { selectBlock(5); return false; } // 6 was pressed.
                if (event.which === 55 || event.which === 103) { selectBlock(6); return false; } // 7 was pressed.
                if (event.which === 56 || event.which === 104) { selectBlock(7); return false; } // 8 was pressed.
                if (event.which === 57 || event.which === 105) { selectBlock(8); return false; } // 9 was pressed.
                // @formatter:on
            }
            if (event.which === 69) { // E for inventory.
                if (byId('inventoryModal').style.display === 'block') {
                    inventoryModal.close();
                } else {
                    inventoryModal.open();
                }
            }
        }
        if (!event.metaKey && !event.shiftKey && !event.altKey && event.ctrlKey && event.which === 83) { // Ctrl+S.
            savePlan();
            return false;
        }
    });

    $(document).on('click', '#blocksHotbar .block', function () {
        selectBlock($(this).parent().index());
    });

    $('#planContainer, #blocksHotbar').bind('mousewheel DOMMouseScroll', function (event) {
        if (!event.ctrlKey) {
            let blockToSelectIndex = $('.selectedBlock').index();
            if (event.originalEvent.wheelDelta > 0) {
                blockToSelectIndex--;
                if (blockToSelectIndex < 0) {
                    blockToSelectIndex = blocksHotbarArr.length - 1;
                }
            } else {
                blockToSelectIndex++;
                if (blockToSelectIndex > blocksHotbarArr.length - 1) {
                    blockToSelectIndex = 0;
                }
            }
            selectBlock(blockToSelectIndex);
        }
        return false;
    });

    initDraggable();

    $('#inventoryModal .inventorySlot.hasBlock').hover(function (event) {
        $mouseOveredBlock = $(this).find('.block');
    }, function (event) {
        $mouseOveredBlock = null;
    });

    $(window).on('resize', resizeWindow);
    resizeWindow();

});

function savePlan () {
    let data = {
        'objectName': $objectName.val(),
        'planWidth':  $columnsNum.val(),
        'planHeight': $rowsNum.val(),
        'planData':   exportPlan(),
    };
    if (typeof objectHash !== 'undefined') {
        data['objectHash'] = objectHash;
        postXhr('/xhr/plan/update', data);
    } else {
        postXhr('/xhr/plan/create', data, function (responseArr) {
            objectHash = responseArr['objectHash'];
            // Redirect to that plan, but don't reload.
            const newUrl = window.location.protocol + '//' + window.location.host + '/plan:' + objectHash + '/';
            window.history.pushState({path: newUrl}, '', newUrl);
        });
    }
}

function clearAll () {
    $planTable.find('td.hasImg').each(function () {
        destroyBlock($(this));
    });
}

function duplicatePlan () {
    let data = {
        'objectName': $('#newObjectName').val(),
        'planWidth':  $columnsNum.val(),
        'planHeight': $rowsNum.val(),
        'planData':   exportPlan(),
    };
    postXhr('/xhr/plan/create', data, function (responseArr) {
        window.location.href = '/plan:' + responseArr['objectHash'] + '/';
    });
}

function deletePlan () {
    let data = {
        'objectHash': objectHash,
    };
    postXhr('/xhr/plan/delete', data, function (responseArr) {
        window.location.reload();
    });
}

function undeletePlan () {
    let data = {
        'objectHash': objectHash,
    };
    postXhr('/xhr/plan/undelete', data, function (responseArr) {
        window.location.reload();
    });
}

function resizeWindow () {
    $inventoryModal.css('left', 'calc((100% - ' + $('#inventoryModal').outerWidth() + 'px) / 2');
}

function updateBlocksHotbarArr () {
    setTimeout(function () {
        blocksHotbarArr = [];
        $('#inventoryModal #hotbar .inventorySlot').each(function () {
            const $divBlock = $(this).find('.block');
            if ($divBlock.length) {
                blocksHotbarArr.push($divBlock.data('block-id'));
            } else {
                blocksHotbarArr.push('');
            }
        });
        initBlocksHotbar();
    }, 0);
}

function initDraggable () {
    $('#inventoryModal .block').draggable({
        revert:         'invalid',
        revertDuration: 50,
        containment:    '#inventoryModal .modal-content',
        helper:         'clone',
        zIndex:         1,
        cursor:         'move',
        cursorAt:       {
            top:  18,
            left: 18
        }
    });

    $('#inventoryModal .inventorySlot').droppable({
        accept:     '.block',
        hoverClass: 'dragOver',
        drop:       function (event, ui) {
            const $this = $(this);
            const originTableId = ui.draggable.closest('#inventory, #hotbar').attr('id');
            const destinationTableId = $this.closest('#inventory, #hotbar').attr('id');
            if (originTableId == 'inventory' && destinationTableId == 'hotbar') { // Dragging from inventory to hotbar.
                // Clone the dragged block into this hotbar slot.
                $this.html(ui.draggable.clone());
                updateBlocksHotbarArr();
            } else if (originTableId == 'hotbar' && destinationTableId == 'inventory') { // Dragging from inventory to hotbar.
                // Destroy hotbar block.
                ui.draggable.remove();
                updateBlocksHotbarArr();
            } else if (originTableId == 'hotbar' && destinationTableId == 'hotbar') { // Dragging from hotbar to hotbar.
                // Clone the dragged block into this hotbar slot, then destroy the old dragged block.
                $this.html(ui.draggable.clone());
                ui.draggable.parent().removeClass('hasBlock');
                ui.draggable.remove();
                updateBlocksHotbarArr();
            } else if (originTableId == 'inventory' && destinationTableId == 'inventory') { // Dragging from inventory to inventory.
                // Do nothing.
            }
        }
    });
}

function exportPlan () {
    let planArr = {};
    $planTable.find('td.hasImg').each(function () {
        const $this = $(this);
        const blockRowNum = parseInt($this.data('row') || 0);
        const blockColumnNum = parseInt($this.data('column') || 0);
        const blockId = parseInt($this.data('block-id') || 0);
        if (blockRowNum && blockColumnNum && blockId) {
            if (!(blockRowNum in planArr)) {
                planArr[blockRowNum] = {};
            }
            planArr[blockRowNum][blockColumnNum] = blockId;
        }
    });
    return JSON.stringify(planArr);
}

function downloadPlan () {
    const encodedUri = encodeURI(exportPlan());
    const link = document.createElement('a');
    link.setAttribute('href', 'data:text/json;charset=utf-8,' + encodedUri);
    link.setAttribute('download', $objectName.val() + '.json');
    document.body.appendChild(link);
    link.click();
}

function importPlan (planJson) {
    initTable(columnsNum, rowsNum);
    const planArr = JSON.parse(planJson);
    for (const rowNum in planArr) {
        if (planArr.hasOwnProperty(rowNum)) {
            for (const columnNum in planArr[rowNum]) {
                if (planArr[rowNum].hasOwnProperty(columnNum)) {
                    const blockId = planArr[rowNum][columnNum];
                    placeBlock($planTable.find('tr:nth-child(' + rowNum + ') td:nth-child(' + columnNum + ')'), blockId);
                }
            }
        }
    }
}

document.addEventListener('contextmenu', event => event.preventDefault()); // Disable right click menu.

let isClickdown = false;

function handleMouseEvent ($this, event) {
    if (!isSpacedown) {
        if (event.which === 2) { // Select block on middle click.
            blocksHotbarArr[selectedBlockIndex] = $this.data('block-id');
            initBlocksHotbar();
        } else if (rightClickToPlace) {
            if (event.which === 1) { // Destroy block on left click.
                destroyBlock($this);
            } else if (event.which === 3) { // Place block on right click.
                placeBlock($this);
            }
        } else {
            if (event.which === 1) { // Place block on left click.
                placeBlock($this);
            } else if (event.which === 3) { // Destroy block on right click.
                destroyBlock($this);
            }
        }
    }
}

function initBlocksHotbar () {
    setCookie('blocksHotbarArr', blocksHotbarArr.join(','), 365);
    let blocksHotbarHtml = '';
    let i = 0;
    for (const blockId of blocksHotbarArr) {
        blocksHotbarHtml += "<div class=\"inventorySlot\">";
        if (blockId != '' && blockId != undefined) {
            const blockDetails = blocksArr[blockId];
            blocksHotbarHtml += " <div class=\"block" + ((selectedBlockIndex == i) ? ' selectedBlock' : '') + " waves-effect btn-flat\" data-block-id=\"" + blockId + "\" title=\"" + blockDetails['title'] + "\">";
            blocksHotbarHtml += "  <img class=\"blockImg\" src=\"/assets/blockItems/" + blockId + ".png\" alt=\"" + blockDetails['label'] + "\">";
            blocksHotbarHtml += " </div>";
        }
        blocksHotbarHtml += "</div>";
        i++;
    }
    $blocksHotbar.html(blocksHotbarHtml);

    let inventoryHotbarHtml = '';
    for (const blockId of blocksHotbarArr) {
        inventoryHotbarHtml += "<div class=\"inventorySlot\">";
        if (blockId != '' && blockId != undefined) {
            const blockDetails = blocksArr[blockId];
            inventoryHotbarHtml += " <div class=\"block\" data-block-id=\"" + blockId + "\" title=\"" + blockDetails['title'] + "\">";
            inventoryHotbarHtml += "  <img class=\"blockImg\" src=\"/assets/blockItems/" + blockId + ".png\" alt=\"" + blockDetails['label'] + "\">";
            inventoryHotbarHtml += " </div>";
        }
        inventoryHotbarHtml += "</div>";
    }
    $('#inventoryModal #hotbar').html(inventoryHotbarHtml);
    selectBlock(selectedBlockIndex);
    initDraggable();
}

function destroyBlock ($td) {
    $td.css('background-image', 'none');
    $td.removeClass('hasImg');
    $td.data('block-id', '');
}

function placeBlock (td, blockId) {
    blockId = typeof blockId !== 'undefined' ? blockId : selectedBlockId;
    td.css('background-image', 'url(/assets/blockTiles/' + blockId + '.png)');
    td.addClass('hasImg');
    td.data('block-id', blockId);
}

function selectBlock (blockToSelectIndex) {
    const $blockToSelect = $('#blocksHotbar .inventorySlot:nth-child(' + (parseInt(blockToSelectIndex) + 1) + ')');
    selectedBlockIndex = blockToSelectIndex;
    selectedBlockId = $blockToSelect.find('.block').data('block-id');
    $('.selectedBlock').removeClass('selectedBlock');
    $blockToSelect.addClass('selectedBlock');
    setCookie('selectedBlockIndex', selectedBlockIndex, 365);
}
