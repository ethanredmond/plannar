let isSpacedown = false;

$(document).ready(function () {

    const planTable = byId('planTable');
    const planContainer = byId('planContainer');

    // Initialize Scroller.
    const render = (function (global) {

        const docStyle = document.documentElement.style;

        let engine;
        if (global.opera && Object.prototype.toString.call(opera) === '[object Opera]') {
            engine = 'presto';
        } else if ('MozAppearance' in docStyle) {
            engine = 'gecko';
        } else if ('WebkitAppearance' in docStyle) {
            engine = 'webkit';
        } else if (typeof navigator.cpuClass === 'string') {
            engine = 'trident';
        }

        const vendorPrefix = {
            trident: 'ms',
            gecko:   'Moz',
            webkit:  'Webkit',
            presto:  'O'
        }[engine];

        const helperElem = document.createElement('div');
        let undef;

        const perspectiveProperty = vendorPrefix + 'Perspective';
        const transformProperty = vendorPrefix + 'Transform';

        if (helperElem.style[perspectiveProperty] !== undef) {

            return function (left, top, zoom) {
                planTable.style[transformProperty] = 'translate3d(' + (-left) + 'px,' + (-top) + 'px,0) scale(' + zoom + ')';
                planTable.style.top = Math.max((planContainer.offsetHeight - (planTable.offsetHeight * zoom)) / 2, 0) + 'px';
                planTable.style.left = Math.max((planContainer.offsetWidth - (planTable.offsetWidth * zoom)) / 2, 0) + 'px';
            };

        } else if (helperElem.style[transformProperty] !== undef) {

            return function (left, top, zoom) {
                planTable.style[transformProperty] = 'translate(' + (-left) + 'px,' + (-top) + 'px) scale(' + zoom + ')';
                planTable.style.top = Math.max((planContainer.offsetHeight - (planTable.offsetHeight * zoom)) / 2, 0) + 'px';
                planTable.style.left = Math.max((planContainer.offsetWidth - (planTable.offsetWidth * zoom)) / 2, 0) + 'px';
            };

        } else {

            return function (left, top, zoom) {
                planTable.style.marginLeft = left ? (-left / zoom) + 'px' : '';
                planTable.style.marginTop = top ? (-top / zoom) + 'px' : '';
                planTable.style.zoom = zoom || '';
            };

        }

    })(this);


    const scroller = new Scroller(render, {
        zooming:                 true,
        penetrationDeceleration: 0.2,
        penetrationAcceleration: 0.2,
        frictionFactor:          0.5,
        minZoom:                 0.1,
        maxZoom:                 3
    });

    const rect = planContainer.getBoundingClientRect();
    scroller.setPosition(rect.left + planContainer.clientLeft, rect.top + planContainer.clientTop);

    // Reflow handling.
    function reflow () {
        scroller.setDimensions(planContainer.offsetWidth, planContainer.offsetHeight, planTable.offsetWidth, planTable.offsetHeight);
    }

    $(window).on('resize', reflow);
    reflow();

    // scroller.zoomTo(0.4);


    function zoomIn () {
        scroller.zoomBy(1.2, true);
    }

    function zoomOut () {
        scroller.zoomBy(0.8, true);
    }

    let mouseCoords;

    $planContainer.mousemove(function (event) {
        mouseCoords = {
            pageX: event.pageX,
            pageY: event.pageY
        };
        if (isSpacedown) {
            scroller.doTouchMove([mouseCoords], event.timeStamp);
        }
    });

    $(document).keydown(function (event) {
        if ($(event.target).closest('input')[0]) {
            return;
        }
        if (!event.metaKey && !event.ctrlKey && !event.shiftKey && !event.altKey) {
            if (event.which === 87 || event.which === 38) { // W or up arrow.
                scroller.scrollBy(0, -150, true);
                return false;
            } else if (event.which === 68 || event.which === 39) { // D or right arrow.
                scroller.scrollBy(150, 0, true);
                return false;
            } else if (event.which === 83 || event.which === 40) { // S or down arrow.
                scroller.scrollBy(0, 150, true);
                return false;
            } else if (event.which === 65 || event.which === 37) { // A or left arrow.
                scroller.scrollBy(-150, 0, true);
                return false;
            } else if (event.which === 187 || event.which === 107) { // Plus key (+).
                zoomIn();
                return false;
            } else if (event.which === 189 || event.which === 109) { // Minus key (-).
                zoomOut();
                return false;
            } else if (event.which === 32) { // Spacebar.
                scroller.doTouchStart([mouseCoords], event.timeStamp);
                isSpacedown = true;
                $planTable.css('cursor', 'move');
                return false;
            }
        }
    });
    $(document).keyup(function (event) {
        if (event.which === 32) { // Spacebar.
            scroller.doTouchEnd(event.timeStamp);
            isSpacedown = false;
            $planTable.css('cursor', 'auto');
            return false;
        }
    });

    $('#planContainer, #blocksHotbar').bind('mousewheel DOMMouseScroll', function (event) {
        if (event.ctrlKey) {
            scroller.doMouseZoom(event.originalEvent.detail ? (event.originalEvent.detail * -120) : event.originalEvent.wheelDelta, event.timeStamp, event.pageX, event.pageY);
        }
        return false;
    });

    $('#zoomOut').on('click', zoomOut);
    $('#zoomIn').on('click', zoomIn);
});


// Initialize Scroller.
// function render () {
//     return function (left, top, zoom) {
//         $planTable.css('transform', 'translate3d(' + (-left) + 'px,' + (-top) + 'px,0) scale(' + zoom + ')');
//         $planTable.css('top', Math.max(($planContainer.height() - ($planTable.height() * zoom)) / 2, 0) + 'px');
//         $planTable.css('left', Math.max(($planContainer.width() - ($planTable.width() * zoom)) / 2, 0) + 'px');
//     };
// }
