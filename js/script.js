$(document).ready(function () {
    M.AutoInit();
    $.widget.bridge('uitooltip', $.ui.tooltip); // https://github.com/Dogfalo/materialize/issues/3653
    $(document).uitooltip({
        content: function () {
            return $(this).prop('title');
        },
        track:   true,
        show:    false,
        hide:    false
    });
    $('#saveButton').click(function () {
        saveUserForm();
    });
});

function postXhr (endpoint, data, successFunc) {
    $.ajax({
        method:  'POST',
        url:     endpoint,
        data:    data,
        success: function (response) {
            console.log("Response: " + response);
            try {
                const responseArr = JSON.parse(response);
                console.log(response);
                console.log(responseArr);
                if (typeof responseArr.status === 'undefined' || responseArr.status === 'error') {
                    statusToast('warning', responseArr.message);
                } else {
                    statusToast('check', responseArr.message);
                    if (typeof successFunc !== 'undefined') {
                        successFunc(responseArr);
                    }
                }
                if (typeof responseArr.fieldName !== 'undefined') {
                    focusElement(responseArr.fieldName);
                }
                if (typeof responseArr.redirectUrl !== 'undefined') {
                    setTimeout(function () {
                        window.location.href = responseArr.redirectUrl;
                    }, 1000);
                }
            } catch (error) {
                statusToast('warning');
                console.log(error);
            }
        },
        error:   function (response) {
            statusToast('warning');
            console.log(response);
        }
    });
}

function statusToast (icon, message) {
    if (message == null) {
        // Set default message.
        if (icon == 'warning') {
            message = 'Error with save :(';
        } else if (icon == 'check') {
            message = 'Success';
        }
    }
    let color = '';
    if (icon == 'warning') {
        color = 'red';
    } else if (icon == 'check') {
        color = 'green';
    }
    M.toast({
        html:    "<i class=\"material-icons\">" + icon + "</i> &nbsp; &nbsp; " + message,
        classes: color + ' rounded lighten-1',
    });
}

function focusElement (id) {
    const $errorField = $('#' + id);
    if ($errorField.length) {
        // Open side panel, if the element is inside it.
        if ($errorField.closest('#slide-out').length) {
            sidePanel.open();
        }
        $errorField.focus();
    }
}

function byId (id) {
    return document.getElementById(id);
}

function setCookie (name, value, days) {
    let expires = '';
    if (days) {
        const date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = '; expires=' + date.toUTCString();
    }
    document.cookie = name + '=' + (value || '') + expires + '; path=/';
}

function getCookie (name) {
    const nameEQ = name + '=';
    const ca = document.cookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1, c.length);
        }
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function eraseCookie (name) {
    document.cookie = name + '=; Max-Age=-99999999;';
}

function showSaveButton () {
    $('#saveButton').show();
}

function doSelectMouseEvent (evt) {
    if (document.hasFocus()) {
        doSelect(evt);
    }
}

function doSelect (evt) {
    if (document.hasFocus()) {
        if (window.event) {
            evt = window.event;
        }
        const srcEl = evt.target || evt.srcElement;
        window.focus(); // This puts the window in the foreground, ready for the Ctrl+C.
        if (typeof document.body.createTextRange != 'undefined') {
            // Internet Explorer.
            const rangeObj = document.body.createTextRange();
            rangeObj.moveToElementText(srcEl);
            rangeObj.select();
        } else {
            // Firefox & Chrome.
            const rangeObj = document.createRange();
            rangeObj.selectNodeContents(srcEl);
            const selectionObj = window.getSelection();
            selectionObj.removeAllRanges();
            selectionObj.addRange(rangeObj);
        }
    }
}

function doDeSelect () {
    if (document.selection) {
        document.selection.empty();
    } else if (window.getSelection) {
        window.getSelection().removeAllRanges();
    }
}

function saveUserForm () {
    let data = {
        'objectHash': objectHash,
        'objectName': $('#objectName').val(),
        'bioText':    $('#bioText').val(),
        'isAdmin':    $('#isAdmin').is(':checked'),
    };
    postXhr('/xhr/user/update', data);
    return false;
}
