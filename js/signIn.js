gapi.load('auth2', function () {
    // Retrieve the singleton for the GoogleAuth library and set up the client.
    auth2 = gapi.auth2.init({
        client_id:    $('meta[name=google-signin-client_id]').attr('content'),
        cookiepolicy: 'single_host_origin',
    });
    auth2.attachClickHandler(document.getElementById('signInBtn'), {}, onSignInSuccess, onSignInError);
});

function onSignInSuccess (googleUser) {
    postXhr('/xhr/sign-in', {
        'idToken': googleUser.getAuthResponse().id_token
    });
}

function onSignInError (error) {
    statusToast('warning', "Sign-in failed :(");
    console.log(JSON.stringify(error, undefined, 2));
}

function signOut () {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
        eraseCookie('_SESSION_STR');
        console.log('User signed out.');
    });
}
