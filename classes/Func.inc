<?php
declare(strict_types=1);

class Func {

    /**
     * Takes an objectId and converts it to the short string used in a url.
     */
    public static function idToHash (int $id) :string {
        // Base 58: https://en.bitcoin.it/wiki/Base58Check_encoding.
        $out = '';
        if (!empty($id)) {
            $codeset = '123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ';
            $base    = strlen($codeset);
            while ($id > 0) {
                $out = substr($codeset, ($id % $base), 1) . $out;
                $id  = (int) floor($id / $base);
            }
        }
        return $out;
    }

    /**
     * Takes the short string used in a url and converts it to an integer objectId.
     */
    public static function hashToId (string $hash) :int {
        // Base 58: https://en.bitcoin.it/wiki/Base58Check_encoding.
        $codeset    = '123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ';
        $base       = strlen($codeset);
        $hash       = preg_replace("/[^0-9a-zA-Z]/", '', $hash);
        $id         = 0;
        $hashLength = strlen($hash);
        for ($i = $hashLength; $i; $i--) {
            $id += strpos($codeset, substr($hash, (-1 * ($i - $hashLength)), 1)) * pow($base, $i - 1);
        }
        return (int) $id;
    }

    /**
     * Strips out special characters from an objectName for use at the end of the object's url.
     */
    public static function dashText (?string $objectName) :string {
        if (function_exists('transliterator_transliterate') && !empty($objectName)) {
            $objectName = transliterator_transliterate('Latin-ASCII', transliterator_transliterate('Latin', $objectName));
        }
        $objectName = preg_replace('/&quot;/', '', $objectName);
        $objectName = preg_replace('/&/', 'and', $objectName);
        $objectName = preg_replace('/\.+/', ' ', $objectName);
        $objectName = preg_replace('/[^\- 0-9a-zA-Z]/', '', $objectName);
        $objectName = str_replace('-', '~', $objectName);
        $objectName = str_replace(' ', '-', $objectName);
        $objectName = str_replace('--', '-', $objectName);
        $objectName = strtolower($objectName);
        return $objectName;
    }

    /**
     * Generate a random string
     *
     * @link https://paragonie.com/b/JvICXzh_jhLyt4y3
     */
    public static function randomString (int $length = 8, bool $isIncludingDigits = true, bool $isIncludingLetters = true, bool $isLowerCaseOnly = true, bool $isIncludingPunctuation = false) :string {
        $charset = '';
        if ($isIncludingLetters) {
            $charset .= 'abcdefghijklmnopqrstuvwxyz';
            if (!$isLowerCaseOnly) {
                $charset .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            }
        }
        if ($isIncludingDigits) {
            $charset .= '12345678901234567890';
        }
        if ($isIncludingPunctuation) {
            $charset .= '!@#$%^&*()_+,.?:;';
        }

        if ($length < 1) {
            // Just return an empty string. Any value < 1 is meaningless.
            return '';
        }
        $charsetMax = strlen($charset) - 1;
        $out        = '';
        for ($i = 0; $i < $length; $i++) {
            $r   = random_int(0, $charsetMax);
            $out .= $charset[$r];
        }
        return $out;
    }

    public static function formatNum (int $num) :string {
        if ($num >= 1000) {
            return number_format($num / 1000, 1) . 'k';
        } else {
            return number_format($num);
        }
    }

    public static function endsWith (string $haystack, string $needle) :bool {
        $length = strlen($needle);
        if ($length == 0) {
            return true;
        }
        return (substr($haystack, -$length) === $needle);
    }

}
