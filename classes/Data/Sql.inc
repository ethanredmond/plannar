<?php
declare(strict_types=1);

namespace Data;

class Sql {

    /* @var \mysqli */
    private static $conn;
    /* @var \mysqli_stmt */
    private static $statement;

    public static function select (string $sql, array $paramsArr = []) :?array {
        $outArr = [];
        if (self::exec($sql, $paramsArr)) {
            $result = self::$statement->get_result();
            if ($result !== false) {
                while ($row = $result->fetch_assoc()) {
                    $outArr[] = $row;
                }
            }
        }
        return $outArr;
    }

    /**
     * Returns the id of the inserted row.
     */
    public static function insert (string $sql, array $paramsArr = []) :int {
        self::exec($sql, $paramsArr);
        return (int) self::$conn->insert_id;
    }

    /**
     * Returns the number of rows changed.
     */
    public static function update (string $sql, array $paramsArr = []) :int {
        self::exec($sql, $paramsArr);
        return self::$conn->affected_rows;
    }

    private static function exec (string $sql, array $paramsArr = []) :bool {
        self::setUpDbConn();
        self::$statement = self::$conn->prepare($sql);
        $latestError     = self::$conn->error;
        if (self::$statement === false || !empty($latestError)) {
            $msgText = '';
            $msgText .= str_replace('You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near', 'SQL syntax error just before:', $latestError);
            $msgText .= "<hr>";
            $msgText .= $sql;
            echo \Endpoints\Page\Msg::html([
                'msgHeading' => "Error",
                'msgText'    => $msgText,
            ]);
            exit;
        }
        if (!empty($paramsArr)) {
            $paramTypes = '';
            foreach ($paramsArr as $paramVal) {
                if (is_int($paramVal)) {
                    $paramTypes .= 'i';
                } elseif (is_float($paramVal)) {
                    $paramTypes .= 'd';
                } else {
                    $paramTypes .= 's';
                }
            }
            self::$statement->bind_param($paramTypes, ...$paramsArr);
        }
        return self::$statement->execute();
    }

    private static function setUpDbConn () :void {
        if (empty(self::$conn)) {
            self::$conn = new \mysqli('localhost', 'server', \Data\LocalParams::$dbServerPassword, 'plannar', 128);
            if (empty(self::$conn) || self::$conn->errno) {
                echo "Connection failed " . self::$conn->errno . ".";
                exit;
            }
            self::$conn->select_db('plannar');
            self::$conn->options(MYSQLI_OPT_INT_AND_FLOAT_NATIVE, true);
            self::$conn->set_charset('utf8');
        }
    }

}
