<?php
declare(strict_types=1);

namespace Data;

class LocalParams {

    /* @var string */
    public static $dbServerPassword;
    /* @var string */
    public static $googleClientId; // Have to add a row to localParams.csv for this: `googleClientId,            999999999999-asdfasfasfasfasfasfasfasdfasfasf`. Get from https://console.cloud.google.com/apis/credentials?orgonly=true&project=plannar-1533397637915&supportedpurview=organizationId

    /* @var string */
    private static $filePath = '/getCredScripts/localParams.csv';

    public static function init () {
        if (($handle = fopen(self::$filePath, 'r')) !== false) {
            while (($varArr = fgetcsv($handle)) !== false) {
                if (!empty($varArr) && count($varArr) > 1) {
                    // If there are comments for the line they would be stored in $varArr[2].
                    $varName = preg_replace('/[^0-9a-zA-Z_]*/', '', $varArr[0]);
                    if (property_exists('\Data\LocalParams', $varName)) {
                        self::$$varName = trim($varArr[1]);
                    }
                }
            }
            fclose($handle);
        }
    }
}

\Data\LocalParams::init();
