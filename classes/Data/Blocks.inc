<?php
declare(strict_types=1);

namespace Data;

class Blocks {

    /**
     * Get label, id and id name from ids.txt (http://piratemc.com/minecraft-json-id-list/)
     * Item imgs from: https://www.minecraftforum.net/forums/mapping-and-modding-java-edition/resource-packs/resource-pack-discussion/1251165-362-icons-minecraft-item-block-icons-download
     *
     * @var array
     *
     * https://minecraft.gamepedia.com/File:BlockCSS.png
     * https://minecraft.gamepedia.com/Template:BlockSprite/doc
     * https://minecraft.gamepedia.com/Module:BlockSprite
     */
    public static $blocksArr = [
        // @formatter:off
         '1'   => ['label' => "Stone",              'idName' => 'minecraft:stone'               ],
         '2'   => ['label' => "Grass",              'idName' => 'minecraft:grass'               ],
         '3'   => ['label' => "Dirt",               'idName' => 'minecraft:dirt'                ],
         '4'   => ['label' => "Cobblestone",        'idName' => 'minecraft:cobblestone'         ],
         '5'   => ['label' => "Oak Wood Plank",     'idName' => 'minecraft:planks'              ],
         '7'   => ['label' => "Bedrock",            'idName' => 'minecraft:bedrock'             ],
        '12'   => ['label' => "Sand",               'idName' => 'minecraft:sand'                ],
        '13'   => ['label' => "Gravel",             'idName' => 'minecraft:gravel'              ],
        '14'   => ['label' => "Gold Ore",           'idName' => 'minecraft:gold_ore'            ],
        '15'   => ['label' => "Iron Ore",           'idName' => 'minecraft:iron_ore'            ],
        '16'   => ['label' => "Coal Ore",           'idName' => 'minecraft:coal_ore'            ],
        '17'   => ['label' => "Oak Wood",           'idName' => 'minecraft:log'                 ],
        '18'   => ['label' => "Oak Leaves",         'idName' => 'minecraft:leaves'              ],
        '20'   => ['label' => "Glass",              'idName' => 'minecraft:glass'               ],
        // '30'   => ['label' => "Cobweb",             'idName' => 'minecraft:web'                 ], // Cobweb is hardly visible atm.
        '41'   => ['label' => "Gold Block",         'idName' => 'minecraft:gold_block'          ],
        '42'   => ['label' => "Iron Block",         'idName' => 'minecraft:iron_block'          ],
        '44'   => ['label' => "Stone Slab",         'idName' => 'minecraft:stone_slab'          ],
        '45'   => ['label' => "Bricks",             'idName' => 'minecraft:brick_block'         ],
        '47'   => ['label' => "Bookshelf",          'idName' => 'minecraft:bookshelf'           ],
        '48'   => ['label' => "Moss Stone",         'idName' => 'minecraft:mossy_cobblestone'   ],
        '49'   => ['label' => "Obsidian",           'idName' => 'minecraft:obsidian'            ],
        '56'   => ['label' => "Diamond Ore",        'idName' => 'minecraft:diamond_ore'         ],
        '57'   => ['label' => "Diamond Block",      'idName' => 'minecraft:diamond_block'       ],
        '58'   => ['label' => "Crafting Table",     'idName' => 'minecraft:crafting_table'      ],
        '73'   => ['label' => "Redstone Ore",       'idName' => 'minecraft:redstone_ore'        ],
        '85'   => ['label' => "Oak Fence",          'idName' => 'minecraft:fence'               ],
        '98'   => ['label' => "Stone Bricks",       'idName' => 'minecraft:stonebrick'          ],
        // @formatter:on
    ];
    /* @var array */
    public static $blockIdsToIdNamesArr = [];
    /* @var array */
    public static $blockTilesPath = '/mnt/ebs1/git/plannar/assets/blockTiles';
    /* @var array */
    public static $blockItemsPath = '/mnt/ebs1/git/plannar/assets/blockItems';

    public static function init () :void {
        self::$blockIdsToIdNamesArr = array_combine(array_keys(self::$blocksArr), array_column(self::$blocksArr, 'idName'));
        foreach (self::$blocksArr as $blockId => $blockDetails) {
            self::$blocksArr[$blockId]['id']    = $blockId;
            self::$blocksArr[$blockId]['title'] = "$blockDetails[label] (#$blockId)<small>$blockDetails[idName]</small>";
        }
    }

}

\Data\Blocks::init();
