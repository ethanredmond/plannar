<?php
declare(strict_types=1);

namespace Endpoints\Page;

class Editor {

    /* @var \Obj\Plan */
    private static $PLAN;
    /* @var int */
    private static $selectedBlockIndex = 0;
    /**
     * Default hotbar
     *
     * @var array
     */
    private static $blocksHotbarArr = [
        '17', // Oak Wood.
        '5', // Oak Wood Plank.
        '20', // Glass.
        '44', // Stone Slab.
        '4', // Cobblestone.
        '1', // Stone.
        '2', // Grass.
        '3', // Dirt.
        '85', // Oak Fence.
    ];

    /**
     * http://www.plannar.com
     * http://www.plannar.com/plan:2/
     * http://www.plannar.com/page/editor?objectHash=2
     */
    public static function html () :string {
        $out = '';

        if (!empty($_GET['objectHash'])) {
            self::$PLAN = \Obj\Plan::get(\Func::hashToId($_GET['objectHash']));
            if (empty(self::$PLAN) || self::$PLAN->isEmpty()) {
                return \Endpoints\Page\Msg::html([
                    'msgHeading' => "Plan not found :(",
                    'msgText'    => "This is not the plan you are looking for.",
                ]);
            }
        }

        $out .= self::head_html();
        $out .= self::body_html();

        return $out;
    }

    private static function head_html () :string {
        self::setBlocksHotBarArrFromCookie();
        self::setSelectedBlockIndexFromCookie();
        $out = '';
        $out .= \Endpoints\Page\Head::html();
        $out .= " <title>" . ((!empty(self::$PLAN->objectName)) ? self::$PLAN->objectName : "New Plan") . " - Plannar</title>";
        $out .= " <script type=\"text/javascript\" src=\"/ext/js/Animate.js\"></script>";
        $out .= " <script type=\"text/javascript\" src=\"/ext/js/Scroller.js\"></script>";
        $out .= " <script>";
        if (!empty(self::$PLAN->objectHash)) {
            $out .= "  let objectHash = '" . self::$PLAN->objectHash . "';";
        } else {
            $out .= "  let objectHash;";
        }
        $out .= "  const blocksArr        = " . json_encode(\Data\Blocks::$blocksArr) . ";";
        $out .= "  let blocksHotbarArr    = " . json_encode(self::$blocksHotbarArr) . ";";
        $out .= "  let selectedBlockIndex = " . self::$selectedBlockIndex . ";";
        $out .= " </script>";
        $out .= " <script type=\"text/javascript\" src=\"/js/editor.js\"></script>";
        $out .= " <script type=\"text/javascript\" src=\"/js/scroller.js\"></script>";
        $out .= "</head>";
        return $out;
    }

    private static function setBlocksHotBarArrFromCookie () :void {
        if (!empty($_COOKIE['blocksHotbarArr'])) {
            $blocksHotbarArr = explode(',', $_COOKIE['blocksHotbarArr']);
            if (!empty($blocksHotbarArr)) {
                array_splice($blocksHotbarArr, 9);
                $blocksHotbarArr = array_pad($blocksHotbarArr, 9, '');
                foreach ($blocksHotbarArr as $key => $blockId) {
                    if (!array_key_exists($blockId, \Data\Blocks::$blocksArr)) {
                        $blocksHotbarArr[$key] = '';
                    }
                }
                if (!empty(array_filter($blocksHotbarArr))) {
                    self::$blocksHotbarArr = $blocksHotbarArr;
                }
            }
        }
    }

    private static function setSelectedBlockIndexFromCookie () :void {
        if (isset($_COOKIE['selectedBlockIndex']) && $_COOKIE['selectedBlockIndex'] > 0 && $_COOKIE['selectedBlockIndex'] < 9) {
            self::$selectedBlockIndex = $_COOKIE['selectedBlockIndex'];
        }
    }

    private static function body_html () :string {
        $out = '';
        $out .= "<body" . ((\Statics::$isDev) ? " class=\"isDev\"" : '') . ">\n";
        $out .= self::editorNavBarDropDown_html();
        $out .= \Endpoints\Page::navBar_html(self::$PLAN->objectName ?? null, true, 'editorNavBarDropDown');
        $out .= self::duplicatePlanModal_html();
        $out .= self::inventoryModal_html();
        $out .= self::confirmDeletePlanModal_html();
        $out .= self::importPlanModal_html();
        $out .= " <div class=\"container\">\n";
        $out .= self::sidePanel_html();
        $out .= self::planContainer_html();
        $out .= self::hotbar_html();
        $out .= " </div>\n";
        $out .= "</body>\n";
        return $out;
    }

    private static function editorNavBarDropDown_html () :string {
        $out = '';
        $out .= " <ul id=\"editorNavBarDropDown\" class=\"dropdown-content\">\n";
        $out .= "  <li><a href=\"\" onclick=\"savePlan(); return false;\" class=\"waves-effect\">Save</a></li>\n";
        $out .= "  <li><a href=\"#duplicatePlanModal\" class=\"waves-effect modal-trigger\">Make a copy...</a></li>\n";
        $out .= "  <li><a href=\"#inventoryModal\" class=\"waves-effect modal-trigger\">Show inventory</a></li>\n";
        $out .= "  <li><a href=\"\" onclick=\"clearAll(); return false;\" class=\"waves-effect\">Clear all</a></li>\n";
        if (!empty(self::$PLAN->isDeleted)) {
            $out .= "  <li><a href=\"\" onclick=\"undeletePlan(); return false;\" class=\"waves-effect\">Undelete</a></li>\n";
        } else {
            $out .= "  <li><a href=\"#confirmDeletePlanModal\" class=\"waves-effect modal-trigger\">Delete...</a></li>\n";
        }
        $out .= "  <li><a href=\"\" onclick=\"downloadPlan(); return false;\" class=\"waves-effect\">Export</a></li>\n";
        $out .= "  <li><a href=\"#importPlanModal\" class=\"waves-effect modal-trigger\">Import...</a></li>\n";
        $out .= " </ul>\n";
        return $out;
    }

    private static function duplicatePlanModal_html () :string {
        $out = '';
        $out .= " <div id=\"duplicatePlanModal\" class=\"modal\">\n";
        $out .= "  <div class=\"modal-content\">\n";
        $out .= "   <a class=\"waves-effect btn-flat modal-close circle\"><i class=\"material-icons\">close</i></a>\n";
        $out .= "   <h5>Copy plan</h5>\n";
        $out .= "   <div class=\"row\">\n";
        $out .= "    <div class=\"input-field col s12\">\n";
        $out .= "     <input id=\"newObjectName\" type=\"text\" value=\"" . ((!empty(self::$PLAN->objectName)) ? "Copy of " . self::$PLAN->objectName : '') . "\" placeholder=\"Copy of Untitled plan\" maxlength=\"50\">\n";
        $out .= "     <label for=\"newObjectName\">Name</label>\n";
        $out .= "    </div>\n";
        $out .= "   </div>\n";
        $out .= "  </div>\n";
        $out .= "  <div class=\"modal-footer\">\n";
        $out .= "   <a href=\"\" onclick=\"return false;\" class=\"modal-close waves-effect btn-flat btn-outlined\">Cancel</a>\n";
        $out .= "   <a href=\"\" onclick=\"duplicatePlan(); return false;\" class=\"modal-close waves-effect btn\">OK</a>\n";
        $out .= "  </div>\n";
        $out .= " </div>\n";
        return $out;
    }

    private static function inventoryModal_html () :string {
        $out             = '';
        $out             .= " <div id=\"inventoryModal\" class=\"modal\">\n";
        $out             .= "  <div class=\"modal-content\">\n";
        $out             .= "   <a class=\"waves-effect btn-flat modal-close circle\"><i class=\"material-icons\">close</i></a>\n";
        $out             .= "   <h5>Inventory</h5>\n";
        $out             .= "   <div id=\"inventory\">\n";
        $i               = 0;
        $blocksArrNoKeys = array_values(\Data\Blocks::$blocksArr);
        while (array_key_exists($i, $blocksArrNoKeys)) {
            $out .= "<div class=\"inventoryRow\">\n";
            for ($j = 0; $j < 9; $j++) {
                $out .= "<div class=\"inventorySlot";
                if (array_key_exists($i, $blocksArrNoKeys)) {
                    $out .= " hasBlock";
                }
                $out .= "\">\n";
                if (array_key_exists($i, $blocksArrNoKeys)) {
                    $blockDetails = $blocksArrNoKeys[$i];
                    $out          .= "<div class=\"block\" data-block-id=\"$blockDetails[id]\" title=\"$blockDetails[title]\" draggable=\"true\">\n";
                    $out          .= " <img class=\"blockImg\" src=\"/assets/blockItems/$blockDetails[id].png\" alt=\"$blockDetails[label]\">\n";
                    $out          .= "</div>\n";
                }
                $out .= "</div>\n";
                $i++;
            }
            $out .= "</div>\n";
            $out .= "<br>\n";
        }
        $out .= "   </div>\n";
        $out .= "   <div id=\"hotbar\">\n";
        $out .= "    <div class=\"inventoryRow\">\n";
        foreach (self::$blocksHotbarArr as $blockId) {
            $out .= "<div class=\"inventorySlot";
            if (!empty($blockId)) {
                $out .= ' hasBlock';
            }
            $out .= "\">\n";
            if (!empty($blockId)) {
                $blockDetails = \Data\Blocks::$blocksArr[$blockId];
                $out          .= "<div class=\"block\" data-block-id=\"$blockId\" title=\"$blockDetails[title]\" draggable=\"true\">\n";
                $out          .= " <img class=\"blockImg\" src=\"/assets/blockItems/$blockId.png\" alt=\"$blockDetails[label]\">\n";
                $out          .= "</div>\n";
            }
            $out .= "</div>\n";
        }
        $out .= "    </div>\n";
        $out .= "   </div>\n";
        $out .= "  </div>\n";
        $out .= "  <div class=\"modal-footer\">\n";
        $out .= "   <a href=\"\" onclick=\"return false;\" class=\"modal-close waves-effect btn-flat btn-outlined\">Close</a>\n";
        $out .= "  </div>\n";
        $out .= " </div>\n";
        return $out;
    }

    private static function confirmDeletePlanModal_html () :string {
        $out = '';
        $out .= " <div id=\"confirmDeletePlanModal\" class=\"modal\">\n";
        $out .= "  <div class=\"modal-content\">\n";
        $out .= "   <p>Delete plan?</p>\n";
        $out .= "  </div>\n";
        $out .= "  <div class=\"modal-footer\">\n";
        $out .= "   <a href=\"\" onclick=\"return false;\" class=\"modal-close waves-effect btn-flat btn-outlined\">Cancel</a>\n";
        $out .= "   <a href=\"\" onclick=\"deletePlan(); return false;\" class=\"modal-close waves-effect btn-flat btn-outlined\">Delete</a>\n";
        $out .= "  </div>\n";
        $out .= " </div>\n";
        return $out;
    }

    private static function importPlanModal_html () :string {
        $out = '';
        $out .= " <div id=\"importPlanModal\" class=\"modal\">\n";
        $out .= "  <div class=\"modal-content\">\n";
        $out .= "   <a class=\"waves-effect btn-flat modal-close circle\"><i class=\"material-icons\">close</i></a>\n";
        $out .= "   <h5>Import plan</h5>\n";
        $out .= "   <div class=\"row\">\n";
        $out .= "    <div class=\"input-field col s12\">\n";
        $out .= "     <input id=\"importedPlanData\" type=\"text\">\n";
        $out .= "     <label for=\"importedPlanData\">Name</label>\n";
        $out .= "    </div>\n";
        $out .= "   </div>\n";
        $out .= "  </div>\n";
        $out .= "  <div class=\"modal-footer\">\n";
        $out .= "   <a href=\"\" onclick=\"return false;\" class=\"modal-close waves-effect btn-flat btn-outlined\">Cancel</a>\n";
        $out .= "   <a href=\"\" onclick=\"importPlan(); return false;\" class=\"modal-close waves-effect btn-flat btn-outlined\">Import</a>\n";
        $out .= "  </div>\n";
        $out .= " </div>\n";
        return $out;
    }

    private static function sidePanel_html () :string {
        $out = '';
        $out .= "  <div id=\"slide-out\" class=\"sidenav\">\n";

        $out .= "   <div class=\"row\">\n";
        $out .= "    <h5 style=\"padding-left: 3.75rem;\">Settings</h5>\n";
        $out .= "   </div>\n";
        $out .= "   <div class=\"divider\"></div>\n";
        $out .= "   <br>\n";

        // $out .= "   <div class=\"row\">\n";
        // $out .= "    <div class=\"input-field col s12\">\n";
        // $out .= "     <i class=\"material-icons prefix\">account_circle</i>\n";
        // $out .= "     <input id=\"objectName\" type=\"text\" value=\"" . ((!empty(self::$PLAN->objectName)) ? self::$PLAN->objectName : '') . "\" placeholder=\"New Plan\" maxlength=\"50\">\n";
        // $out .= "     <label for=\"objectName\">Name</label>\n";
        // $out .= "    </div>\n";
        // $out .= "   </div>\n";

        $out .= "   <div class=\"row\" style=\"padding-left: 3rem;\">\n";
        $out .= "    <div class=\"input-field col s12 m6\">\n";
        $out .= "     <input class=\"percentageInput\" type=\"number\" name=\"columnsNum\" id=\"columnsNum\" value=\"" . (self::$PLAN->planWidth ?? 25) . "\" min=\"1\" max=\"100\">\n";
        $out .= "     <label for=\"columnsNum\">Width</label>\n";
        $out .= "    </div>\n";
        $out .= "    <div class=\"input-field col s12 m6\">\n";
        $out .= "     <input class=\"percentageInput\" type=\"number\" name=\"rowsNum\" id=\"rowsNum\" value=\"" . (self::$PLAN->planHeight ?? 25) . "\" min=\"1\" max=\"100\">\n";
        $out .= "     <label for=\"rowsNum\">Height</label>\n";
        $out .= "    </div>\n";
        $out .= "   </div>\n";

        $out .= "   <div class=\"row\" style=\"padding-left: 3rem;\">\n";
        $out .= "    <div class=\"input-field col s12\">\n";
        $out .= "     <label>\n";
        $out .= "      <input type=\"checkbox\" class=\"filled-in\" id=\"rightClickToPlace\" name=\"rightClickToPlace\"/>\n";
        $out .= "      <span>Right click to place</span>\n";
        $out .= "     </label>\n";
        $out .= "    </div>\n";
        $out .= "   </div>\n";
        $out .= "   <br>\n";

        $out .= "  </div>\n";
        return $out;
    }

    private static function planContainer_html () :string {
        $out = '';
        $out .= "  <div id=\"planContainer\">";
        $out .= "   <table id=\"planTable\" cellspacing=\"0\" cellpadding=\"0\">";
        for ($rowNum = 1; $rowNum <= (self::$PLAN->planHeight ?? 25); $rowNum++) {
            $out .= "<tr class=\"planRow$rowNum\" data-row=\"$rowNum\">";
            for ($columnNum = 1; $columnNum <= (self::$PLAN->planWidth ?? 25); $columnNum++) {
                $out .= "<td class=\"planColumn$columnNum planRow$rowNum";
                if (
                    !empty(self::$PLAN->planData)
                    &&
                    array_key_exists($rowNum, self::$PLAN->planData)
                    &&
                    array_key_exists($columnNum, self::$PLAN->planData[$rowNum])
                    &&
                    array_key_exists(self::$PLAN->planData[$rowNum][$columnNum], \Data\Blocks::$blocksArr)
                ) {
                    $blockId = self::$PLAN->planData[$rowNum][$columnNum];
                    $out     .= " hasImg\"";
                    $out     .= " style=\"background-image: url(/assets/blockTiles/$blockId.png);\"";
                    $out     .= " data-block-id=\"$blockId";
                }
                $out .= '"';
                $out .= " data-column=\"$columnNum\"";
                $out .= " data-row=\"$rowNum\"";
                $out .= ">";
                $out .= "</td>";
            }
            $out .= "</tr>";
        }
        $out .= "   </table>";
        $out .= "  </div>";
        return $out;
    }

    private static function hotbar_html () :string {
        $out = '';
        $out .= "  <div id=\"hotbarContainer\">";
        $out .= "   <div id=\"blocksHotbar\">";
        $i   = 0;
        foreach (self::$blocksHotbarArr as $blockId) {
            $out .= "<div class=\"inventorySlot" . ((self::$selectedBlockIndex == $i) ? ' selectedBlock' : '');
            if (!empty($blockId)) {
                $out .= ' hasImg';
            }
            $out .= "\">";
            $out .= " <div class=\"block";
            if (!empty($blockId)) {
                $out .= ' waves-effect btn-flat';
            }
            $out .= '"';
            if (!empty($blockId)) {
                $blockDetails = \Data\Blocks::$blocksArr[$blockId];
                $out          .= " data-block-id=\"$blockId\" title=\"$blockDetails[title]\"";
            }
            $out .= '>';
            if (!empty($blockId)) {
                $out .= "  <img class=\"blockImg\" src=\"/assets/blockItems/$blockId.png\" alt=\"$blockDetails[label]\">";
            }
            $out .= " </div>";
            $out .= "</div>";
            $i++;
        }
        $out .= "   </div>";
        $out .= "  </div>";
        return $out;
    }

}
