<?php
declare(strict_types=1);

namespace Endpoints\Page;

class Home {

    /**
     * http://www.plannar.com/
     * http://www.plannar.com/home
     * http://www.plannar.com/page/home/
     */
    public static function html () :string {
        $out = '';
        $out .= self::head_html();
        $out .= self::body_html();
        return $out;
    }

    private static function head_html () :string {
        $out = '';
        $out .= \Endpoints\Page\Head::html();
        $out .= " <title>Home - Plannar</title>";
        $out .= "</head>";
        return $out;
    }

    private static function body_html () :string {
        $out = '';
        $out .= "<body" . ((\Statics::$isDev) ? " class=\"isDev\"" : '') . ">";
        $out .= \Endpoints\Page::navBar_html("Home");
        $out .= " <div class=\"container\" data-simplebar>";
        $out .= "  <br>";
        $out .= "  <div class=\"row\">";

        $planIdsArr = array_column(\Data\Sql::select("SELECT objectId FROM plan WHERE isDeleted = 0"), 'objectId');
        $PLAN_arr   = \Obj\Plan::getArray($planIdsArr);
        $out        .= \PlanList::html($PLAN_arr);

        $out .= "  </div>";
        $out .= " </div>";
        $out .= "</body>";
        return $out;
    }

}
