<?php
declare(strict_types=1);

namespace Endpoints\Page;

class SignIn {

    /**
     * http://www.plannar.com/sign-in
     * http://www.plannar.com/page/sign-in
     */
    public static function html () :string {
        if (!empty($_COOKIE['_SESSION_STR'])) {
            $objectId = \Data\Sql::select("SELECT objectId FROM user WHERE session_str = ?", [$_COOKIE['_SESSION_STR']]);
            if (!empty($objectId)) {
                header('Location: /user:' . \Func::idToHash((int) $objectId) . '/');
                exit;
            } else {
                unset($_COOKIE['_SESSION_STR']);
                setcookie('_SESSION_STR', '', $_SERVER['REQUEST_TIME'] - \Statics::$secs_day);
            }
        }
        $out = '';
        $out .= self::head_html();
        $out .= self::body_html();
        return $out;
    }

    public static function head_html () :string {
        $out = '';
        $out .= \Endpoints\Page\Head::html();
        $out .= " <title>Sign in - Plannar</title>";
        $out .= " <meta name=\"google-signin-client_id\" content=\"" . \Data\LocalParams::$googleClientId . ".apps.googleusercontent.com\">";
        $out .= " <script src=\"https://apis.google.com/js/api:client.js\"></script>";
        $out .= " <script type=\"text/javascript\" src=\"/js/signIn.js\"></script>";
        $out .= "</head>";
        return $out;
    }

    public static function body_html () :string {
        $out = '';
        $out .= "<body" . ((\Statics::$isDev) ? " class=\"isDev\"" : '') . ">";
        $out .= " <div id=\"signInRow\" class=\"row\">";
        if (\Statics::$isDev) {
            $out .= "  <div id=\"signInCol\" class=\"col s3 offset-s2 white valign-wrapper z-depth-2\">";
        } else {
            $out .= "  <div id=\"signInCol\" class=\"col s3 offset-s2 pink darken-1 valign-wrapper z-depth-2\">";
        }
        $out .= "   <div class=\"section white-text center-align\">";
        $out .= "    <h4>Sign in to Plannar</h4>";
        $out .= "    <p>Sign in with your Google account to access Plannar.<br>No registration required.</p>";
        $out .= "    <br>";
        $out .= "    <a id=\"signInBtn\" class=\"waves-effect waves-dark btn white grey-text text-darken-4 hoverable\"><img src=\"/assets/googleLogo.png\" class=\"left\">Sign in with Google</a>"; // https://developers.google.com/identity/branding-guidelines
        $out .= "   </div>";
        $out .= "  </div>";
        $out .= " </div>";
        $out .= " <iframe id=\"hiddenIframe\"></iframe>";
        $out .= \Endpoints\Page::backgroundImg_html();
        $out .= "</body>";
        return $out;
    }

    /**
     * http://www.plannar.com/xhr/sign-in
     */
    public static function xhr_run () :string {
        if (!empty($_POST['idToken'])) {
            $client = new \Google_Client();
            $payload = $client->verifyIdToken($_POST['idToken']);
            file_put_contents('/tmp/log.txt', print_r($payload, true) . " " . __FILE__ . " " . __LINE__ . "\n", FILE_APPEND);
            if ($payload) {
                $googleId    = $payload['sub'];
                $sqlArr      = \Data\Sql::select("SELECT objectId FROM user WHERE googleId = ? LIMIT 1", [$googleId]);
                $session_str = \Func::randomString(128, true, true, false, false);
                if (!empty($sqlArr) && !empty($sqlArr[0]['objectId'])) {
                    // Update user.
                    $objectId = $sqlArr[0]['objectId'];
                    \Data\Sql::update("UPDATE user SET session_str = ?, session_startUtcYmdHms = NOW(), session_endUtcYmdHms = ADDDATE(NOW(), INTERVAL 30 DAY) WHERE objectId = ?", [
                        $session_str,
                        $objectId,
                    ]);
                    $redirectUrl = '/';
                } else {
                    // Create user.
                    $userName    = "$payload[given_name]$payload[family_name]";
                    $objectId      = \Data\Sql::insert(<<<SQL
                        INSERT INTO user (
                          googleId,
                          loginEmailAddress,
                          objectName,
                          session_str,
                          session_startUtcYmdHms,
                          session_endUtcYmdHms
                        ) VALUES (
                          ?,
                          ?,
                          ?,
                          ?,
                          NOW(),
                          ADDDATE(NOW(), INTERVAL 30 DAY)
                        )
SQL
                        , [
                            $googleId,
                            $payload['email'],
                            $payload['payload'],
                            $userName,
                            $session_str,
                        ]
                    );
                    $redirectUrl = '/user:' . \Func::idToHash($objectId) . '/';
                }
                setcookie('_SESSION_STR', $session_str, $_SERVER['REQUEST_TIME'] + \Statics::$secs_mth, '/');
                return json_encode(['status' => 'success', 'message' => "Login successful!", 'redirectUrl' => $redirectUrl]);
            } else {
                return json_encode(['status' => 'error', 'message' => "Error with sign-in :( Try again in a few minutes."]);
            }
        } else {
            return json_encode(['status' => 'error', 'message' => "Error with sign-in :( Try again in a few minutes."]);
        }
    }

}
