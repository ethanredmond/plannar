<?php
declare(strict_types=1);

namespace Endpoints\Page;

class User {

    /* @var array */
    private static $USER;

    /**
     * http://www.plannar.com/user:2/
     * http://www.plannar.com/page/user/?objectId=2
     */
    public static function html () :string {
        if (!empty($_GET['objectHash'])) {
            self::$USER = \Obj\User::get(\Func::hashToId($_GET['objectHash']));
            if (self::$USER->isEmpty()) {
                return \Endpoints\Page::msg404();
            }
        } else {
            header("HTTP/1.0 404 Not Found");
            exit;
        }
        $out = '';
        $out .= self::head_html();
        $out .= self::body_html();
        return $out;
    }

    private static function head_html () :string {
        $out = '';
        $out .= \Endpoints\Page\Head::html();
        $out .= " <script>\n";
        $out .= "  const objectHash = '" . self::$USER->objectHash . "';\n";
        $out .= " </script>\n";
        $out .= " <title>" . self::$USER->objectName . " - Plannar</title>\n";
        $out .= "</head>\n";
        return $out;
    }

    private static function body_html () :string {
        $out = '';
        $out .= "<body" . ((\Statics::$isDev) ? " class=\"isDev\"" : '') . ">\n";
        $out .= \Endpoints\Page::navBar_html(self::$USER->objectName);
        $out .= " <div id=\"userTab\" class=\"container\" data-simplebar>\n";
        $out .= "  <br>\n";

        $out .= "  <div class=\"card\">\n";
        $out .= "   <div class=\"card-content white-text\">\n";
        $out .= "    <div class=\"row\">\n";

        $out .= "     <div class=\"col m3\">\n";
        $out .= "     </div>\n";

        $out .= "     <div class=\"col m9\">\n";

        $out .= "      <div class=\"row\">\n";
        $out .= "       <div class=\"input-field col s12\">\n";
        $out .= "        <input id=\"userName\" type=\"text\" value=\"" . htmlspecialchars((string) self::$USER->objectName) . "\" oninput=\"showSaveButton();\">\n";
        $out .= "        <label for=\"userName\">Username</label>\n";
        $out .= "       </div>\n";
        $out .= "      </div>\n";

        $out .= "      <div class=\"row\">\n";
        $out .= "       <div class=\"input-field col s12\">\n";
        $out .= "        <textarea id=\"bioText\" class=\"materialize-textarea\" type=\"text\" oninput=\"showSaveButton();\">" . htmlspecialchars((string) self::$USER->bioText) . "</textarea>\n";
        $out .= "        <label for=\"bioText\">Bio</label>\n";
        $out .= "       </div>\n";
        $out .= "      </div>\n";

        $out .= "      <div class=\"row checkboxRow\">\n";
        $out .= "       <label>\n";
        $out .= "        <input id=\"isAdmin\" type=\"checkbox\" class=\"filled-in\" onchange=\"showSaveButton();\" checked=\"" . (!empty(self::$USER->isAdmin) ? 'checked' : '') . "\" />\n";
        $out .= "        <span>Admin</span>\n";
        $out .= "       </label>\n";
        $out .= "      </div>\n";

        $out .= "     </div>\n";

        $out .= "    </div>\n";
        $out .= "   </div>\n";
        $out .= "  </div>\n";

        $planIdsArr = array_column(\Data\Sql::select("SELECT objectId FROM plan WHERE userId = ? AND isDeleted = 0", [self::$USER->objectId]), 'objectId');
        $PLAN_arr   = \Obj\Plan::getArray($planIdsArr);
        $out        .= \PlanList::html($PLAN_arr);

        $out .= "  <div id=\"saveButtonRow\">\n";
        $out .= "   <a id=\"saveButton\" class=\"waves-effect waves-light btn-large\">Save</a>\n";
        $out .= "  </div>\n";

        $out .= " </div>\n";

        $out .= " <div id=\"plansTab\" class=\"container\">\n";
        $out .= " </div>\n";

        $out .= " <div id=\"starredPlansTab\" class=\"container\">\n";
        $out .= " </div>\n";

        $out .= "</body>\n";
        return $out;
    }

    /**
     * curl --data "objectHash=2" http://www.plannar.com/xhr/user/update
     * curl --data "objectHash=2&objectName=Test" http://www.plannar.com/xhr/user/update
     * curl --data "objectHash=2&objectName=Test&userWidth=asdf" http://www.plannar.com/xhr/user/update
     * curl --data "objectHash=2&objectName=Test&userWidth=50" http://www.plannar.com/xhr/user/update
     * curl --data "objectHash=2&objectName=Test&userWidth=50&userHeight=asdf" http://www.plannar.com/xhr/user/update
     * curl --data "objectHash=2&objectName=Test&userWidth=50&userHeight=50" http://www.plannar.com/xhr/user/update
     * curl --data "objectHash=2&objectName=Test&userWidth=50&userHeight=50&userData=asdf" http://www.plannar.com/xhr/user/update
     */
    public static function update () :string {
        if (empty($_POST['objectHash'])) {
            return json_encode(['status' => 'error', 'message' => "No user found :("]);
        }
        $fieldErrorJson = \Endpoints\Xhr::validatePostVars('user');
        if (!empty($fieldErrorJson)) {
            return $fieldErrorJson;
        }

        $objectId = \Func::hashToId($_POST['objectHash']);
        $outArr   = \Data\Sql::select("SELECT objectId, isDeleted FROM user WHERE objectId = ?", [$objectId]);
        if (empty($outArr[0]) || empty($outArr[0]['objectId'])) { // No user exists.
            return json_encode(['status' => 'error', 'message' => "No user found :("]);
        } elseif (!empty($outArr[0]['isDeleted'])) { // Is deleted.
            return json_encode(['status' => 'error', 'message' => "This user's deleted :( <a onclick=\"alert('TODO :P');\">Undelete?</a>"]);
        }

        $paramsArr = [];
        $sql       = "UPDATE user SET ";
        if (!empty($_POST['userName'])) {
            $paramsArr[] = $_POST['userName'];
            $sql         .= 'userName = ?, ';
        }
        if (!empty($_POST['bioText'])) {
            $paramsArr[] = $_POST['bioText'];
            $sql         .= 'bioText = ?, ';
        }
        if (isset($_POST['isAdmin'])) {
            $paramsArr[] = ($_POST['isAdmin'] == 'true');
            $sql         .= 'isAdmin = ?, ';
        }
        $sql = rtrim($sql, ', ');
        $sql .= ' WHERE objectId = ?';
        if (empty($paramsArr)) {
            return json_encode(['status' => 'error', 'message' => "Nothing to update."]);
        }
        $paramsArr[] = $objectId;
        \Data\Sql::update($sql, $paramsArr);
        return json_encode(['status' => 'success', 'message' => 'Saved']);
    }

}
