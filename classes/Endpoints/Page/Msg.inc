<?php
declare(strict_types=1);

namespace Endpoints\Page;

use Endpoints\Page;

class Msg {

    public static function html (array $paramsArr) :string {
        if (\Statics::$isCli || \Statics::$isAjax) {
            throw new \Exception("$paramsArr[msgHeading]: $paramsArr[msgText]");
        }
        $out = '';
        $out .= \Endpoints\Page\Head::html();
        $out .= "<title>$paramsArr[msgHeading] - Plannar</title>";
        $out .= "</head>";
        $out .= "<body" . ((\Statics::$isDev) ? " class=\"isDev\"" : '') . ">";
        $out .= " <nav>";
        $out .= "  <div class=\"nav-wrapper\">";
        $out .= "   <ul class=\"left\">";
        $out .= "    <li><a class=\"waves-effect modal-trigger\"><i class=\"material-icons\">home</i></a></li>";
        $out .= "   </ul>";
        $out .= "   <a href=\"\" onclick=\"return false;\" class=\"brand-logo center\">$paramsArr[msgHeading]</a>";
        $out .= "  </div>";
        $out .= " </nav>";
        $out .= " <div class=\"container\">";
        $out .= "  <br>";
        $out .= "  <div class=\"row\">";
        $out .= "   <div class=\"col s12 m8 offset-m2\">";
        $out .= "    <div class=\"card\">";
        $out .= "     <div class=\"card-content\">";
        $out .= "      <span class=\"card-title\">$paramsArr[msgHeading]</span>";
        $out .= "      <p>$paramsArr[msgText]</p>";
        if (\Statics::$isDev) {
            $out .= self::devBacktrace();
        }
        $out .= "     </div>";
        $out .= "     <div class=\"card-action\">";
        $out .= "      <a href=\"\" onclick=\"window.location.reload(); return false;\">Reload</a>";
        // $out .= "      <a href=\"/home\">Go to homepage</a>";
        $out .= "     </div>";
        $out .= "    </div>";
        $out .= "   </div>";
        $out .= "  </div>";
        $out .= " </div>";
        $out .= Page::backgroundImg_html();
        $out .= "</body>";
        return $out;
    }

    /**
     * Show the files that led to this error/warning to help with debugging. (Only on dev)
     */
    private static function devBacktrace () :string {
        $filesArr = debug_backtrace();
        $out = '';
        $out .= "<br>";
        $out .= "<div class=\"devErrorBox\">";
        $out .= "Dev trace: (most recent first)";
        $out .= "<div style=\"font-size: 0.9rem;\">";
        for ($i = 1; $i <= 9; $i++) {
            if (!empty($filesArr[$i]) && !empty($filesArr[$i]['file']) && strpos($filesArr[$i]['file'], '/classes/Endpoints/Page/Msg.inc') === false) {
                $out .= "<span " . \Statics::$selectableCodeAttr . " style=\"border-left: none !important; padding: 0 !important; border-radius: 0 !important;\">" . str_replace('/mnt/ebs1/git/plannar', '', $filesArr[$i]['file']) . ":{$filesArr[$i]['line']}</span><br>";
            }
        }
        $out .= "</div>";
        $out .= "</div>";
        return $out;
    }

}
