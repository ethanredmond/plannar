<?php
declare(strict_types=1);

namespace Endpoints\Page;

class Head {

    public static function html () :string {
        $out = '';
        $out .= "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n";
        $out .= "<head profile=\"http://a9.com/-/spec/opensearch/1.1/\">\n";
        $out .= " <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n";
        $out .= " <meta name=\"content-style-type\" content=\"text/css\">\n";
        $out .= " <meta name=\"content-script-type\" content=\"text/javascript\">\n";
        $out .= " <meta http-equiv=\"content-language\" content=\"en, eb-gb\">\n";
        $out .= " <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n";
        $out .= " <link rel=\"stylesheet\" type=\"text/css\" href=\"https://unpkg.com/simplebar@latest/dist/simplebar.css\" />";
        $out .= " <link rel=\"stylesheet\" type=\"text/css\" href=\"https://fonts.googleapis.com/icon?family=Material+Icons\">\n";
        $out .= " <link rel=\"stylesheet\" type=\"text/css\" href=\"https://fonts.googleapis.com/css?family=Roboto\">\n";
        $out .= " <link rel=\"stylesheet\" type=\"text/css\" href=\"/ext/sass/materialize.css\" media=\"screen,projection\"/>\n";
        $out .= " <link rel=\"stylesheet\" type=\"text/css\" href=\"//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css\">\n";
        $out .= " <link rel=\"stylesheet\" type=\"text/css\" href=\"/sass/style.css\">\n";
        $out .= " <script type=\"text/javascript\" src=\"https://unpkg.com/simplebar@latest/dist/simplebar.js\"></script>";
        $out .= " <script type=\"text/javascript\" src=\"/ext/js/materialize.js\"></script>\n";
        $out .= " <script type=\"text/javascript\" src=\"/ext/js/jquery-3.3.1.min.js\"></script>\n";
        $out .= " <script type=\"text/javascript\" src=\"https://code.jquery.com/ui/1.12.1/jquery-ui.js\"></script>\n";
        $out .= " <script type=\"text/javascript\" src=\"/js/script.js\"></script>\n";
        return $out;
    }

}