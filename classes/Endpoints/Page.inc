<?php
declare(strict_types=1);

namespace Endpoints;

class Page {

    /**
     * Uses url rewriting like: https://www.plannar.com/page/endpoint?qsvar=1
     *
     * @var array
     */
    public static $endpoints = [
        'editor'  => '\Endpoints\Page\Editor::html',
        'home'    => '\Endpoints\Page\Home::html',
        'sign-in' => '\Endpoints\Page\SignIn::html',
        'user'    => '\Endpoints\Page\User::html',
        'msg-404' => '\Endpoints\Page::msg404',
    ];

    public static function msg404 () :string {
        return \Endpoints\Page\Msg::html([
            'msgHeading' => "404",
            'msgText'    => "Sorry... we couldn't find the page you were looking for ¯\_(ツ)_/¯",
        ]);
    }

    public static function backgroundImg_html () :string {
        if (\Statics::$isDev) { // Don't show images on dev.
            return '';
        }
        return "<img src=\"" . \Statics::$backgroundsArr[array_rand(\Statics::$backgroundsArr)] . "\" id=\"backgroundImg\" draggable=\"false\">";
    }

    public static function navBar_html (?string $titleStr, bool $isEditable = false, string $dataTargetStr = null) :string {
        $out = '';
        $out .= " <nav>";
        $out .= "  <div class=\"nav-wrapper\">";
        $out .= "   <a href=\"\" onclick=\"return false;\"" . ((!empty($dataTargetStr)) ? " data-target=\"$dataTargetStr\"" : '') . " class=\"sidenav-trigger" . ((!empty($dataTargetStr)) ? ' dropdown-trigger' : '') . "\"><i class=\"material-icons\">menu</i></a>";
        $out .= "   <a href=\"/\" class=\"logo\"></a>";
        if ($isEditable) {
            $out .= "   <div class=\"input-field\">\n";
            $out .= "    <input id=\"objectName\" type=\"text\" value=\"" . ((!empty($titleStr)) ? htmlentities($titleStr) : '') . "\" placeholder=\"Untitled plan\" maxlength=\"50\" autocomplete=\"off\"" . ((empty($titleStr)) ? ' autofocus' : '') . ">\n";
            $out .= "   </div>\n";
        } else {
            $out .= "   <a href=\"\" onclick=\"return false;\" class=\"brand-logo center\">" . htmlentities($titleStr) . "</a>";
        }
        $out .= "  </div>";
        $out .= " </nav>";
        return $out;
    }

}
