<?php
declare(strict_types=1);

namespace Endpoints;

class Xhr {

    /**
     * Uses url rewriting like:  https://www.plannar.com/xhr/endpoint?qsvar=1
     *
     * @var array
     */
    public static $endpoints = [
        'plan/create'   => '\Endpoints\Xhr\Plan::create',
        'plan/update'   => '\Endpoints\Xhr\Plan::update',
        'plan/delete'   => '\Endpoints\Xhr\Plan::delete',
        'plan/undelete' => '\Endpoints\Xhr\Plan::undelete',
        'sign-in'       => '\Endpoints\Page\SignIn::xhr_run',
        'user/update'   => '\Endpoints\Page\User::update',
    ];

    public static function validateFieldFromDataType ($fieldValue, string $fieldName, string $dataType) :?string {
        if ($dataType == 'x' || strpos($dataType, 's') === 0) {
            if (!is_string($fieldValue)) {
                return json_encode(['status' => 'error', 'message' => "\"$fieldName\" must be a string.", 'fieldName' => $fieldName]);
            }
            if (strpos($dataType, 's') === 0) {
                $fieldLength = substr($dataType, 1);
                if (mb_strlen($fieldValue) > $fieldLength) {
                    return json_encode(['status' => 'error', 'message' => "\"$fieldName\" can't be longer than $fieldLength characters.", 'fieldName' => $fieldName]);
                }
            }
        } elseif ($dataType == 'b') {
            if ($fieldValue != 'true' && $fieldValue != 'false') {
                return json_encode(['status' => 'error', 'message' => "\"$fieldName\" must be a boolean.", 'fieldName' => $fieldName]);
            }
        } elseif ($dataType == 'Y' || $dataType == 'i') {
            if (!is_numeric($fieldValue)) {
                return json_encode(['status' => 'error', 'message' => "\"$fieldName\" must be a number.", 'fieldName' => $fieldName]);
            } elseif ($fieldValue < 0) {
                return json_encode(['status' => 'error', 'message' => "\"$fieldName\" can't be negative.", 'fieldName' => $fieldName]);
            }
            // https://dev.mysql.com/doc/refman/8.0/en/integer-types.html
            $fieldMax = null;
            if ($dataType == 'Y') {
                $fieldMax = 65535;
            } elseif ($dataType == 'i') {
                $fieldMax = 4294967295;
            }
            if ($fieldValue > $fieldMax) {
                return json_encode(['status' => 'error', 'message' => "\"$fieldName\" can't be greater than $fieldMax.", 'fieldName' => $fieldName]);
            }
        } // TODO: Datetime validation.
        return null;
    }

    public static function validatePostVars (string $tableName) :?string {
        $schemaClassName = '\\Schema\\' . ucfirst($tableName);
        foreach ($schemaClassName::$objectFieldsArr as $fieldName => $fieldDetailsArr) {
            if (!$fieldDetailsArr['isUpdated'] && isset($_POST[$fieldName])) {
                return json_encode(['status' => 'error', 'message' => "\"$fieldName\" can't be updated.", 'fieldName' => $fieldName]);
            } elseif ($fieldDetailsArr['isUpdated']) {
                if ($fieldDetailsArr['isRequired'] && (!isset($_POST[$fieldName]) || $_POST[$fieldName] == '')) {
                    return json_encode(['status' => 'error', 'message' => "\"$fieldName\" can't be empty.", 'fieldName' => $fieldName]);
                } else {
                    $fieldErrorJson = self::validateFieldFromDataType($_POST[$fieldName] ?? null, $fieldName, $fieldDetailsArr['dataType']);
                    if (!empty($fieldErrorJson)) {
                        return $fieldErrorJson;
                    }
                }
            }
        }
        return null;
    }

}
