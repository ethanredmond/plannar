<?php
declare(strict_types=1);

namespace Endpoints\Cli;

class Tests {

    /*
    php /mnt/ebs1/git/plannar/endpoints/endpointCli.phpx validateblocks
    cli validateblocks
     */
    public static function validateBlocks () :void {
        $errorsArr = [];

        $blockItemFiles = glob(\Data\Blocks::$blockItemsPath . '/*.*');
        $blockTileFiles = glob(\Data\Blocks::$blockTilesPath . '/*.*');

        // Check if the blocks in $blocksArr exist on the file system.
        foreach (\Data\Blocks::$blocksArr as $blockId => $blockDetails) {
            if (!in_array(\Data\Blocks::$blockItemsPath . "/$blockId.png", $blockItemFiles)) {
                $errorsArr["These blocks were found in \$blocksArr, but not in /assets/blockItems/:"][] = $blockId;
            }
            if (!in_array(\Data\Blocks::$blockTilesPath . "/$blockId.png", $blockTileFiles)) {
                $errorsArr["These blocks were found in \$blocksArr, but not in /assets/blockTiles/:"][] = $blockId;
            }
        }

        // Check that the blocks on the file system exist in $blocksArr.
        foreach ($blockItemFiles as $itemFileName) {
            if (!array_key_exists(pathinfo($itemFileName, PATHINFO_FILENAME), \Data\Blocks::$blocksArr)) {
                $errorsArr["These files were found in /assets/blockItems/, but not in \$blocksArr:"][] = $itemFileName;
            }
        }
        foreach ($blockTileFiles as $tileFileName) {
            if (!array_key_exists(pathinfo($tileFileName, PATHINFO_FILENAME), \Data\Blocks::$blocksArr)) {
                $errorsArr["These files were found in /assets/blockTiles/, but not in \$blocksArr:"][] = $tileFileName;
            }
        }

        // Check against the external source.
        $extBlockData = self::getExtBlockData();
        foreach (\Data\Blocks::$blocksArr as $blockId => $blockDetails) {
            if (!array_key_exists($blockId, $extBlockData)) {
                $errorsArr["These ids are incorrect, check them against ids.txt:"][] = $blockId;
            }
            if (!empty($extBlockData[$blockId])) {
                if ($blockDetails['idName'] != $extBlockData[$blockId]['idName']) {
                    $errorsArr["These id names are incorrect, check them against ids.txt:"][] = "The id name for $blockId is set to $blockDetails[idName], but should be {$extBlockData[$blockId]['idName']}.";
                }
                if ($blockDetails['label'] != $extBlockData[$blockId]['label']) {
                    $errorsArr["These labels are incorrect, check them against ids.txt:"][] = "The label for $blockId is set to $blockDetails[label], but should be {$extBlockData[$blockId]['label']}.";
                }
            }
        }

        // Check $blocksArr is ordered correctly.
        $unsortedIds = array_keys(\Data\Blocks::$blocksArr);
        $sortedIds   = $unsortedIds;
        sort($sortedIds);
        if ($sortedIds !== $unsortedIds) {
            $errorsArr[] = "\$blocksArr is incorrectly ordered.";
        }

        if (!empty($errorsArr)) {
            foreach ($errorsArr as $key => $value) {
                echo "\n    " . \Statics::$consoleFailed . ' ';
                if (is_array($value)) {
                    echo "$key\n\n";
                    foreach ($value as $item) {
                        echo "      - $item\n";
                    }
                    echo "\n";
                } else {
                    echo "$value\n\n";
                }
            }
        } else {
            echo "\n    " . \Statics::$consolePassed . "\n\n";
        }
    }

    private static function getExtBlockData () :array {
        $idsText = file_get_contents('/mnt/ebs1/git/plannar/classes/Endpoints/Cli/ids.txt');
        if (empty($idsText)) {
            echo "\n    " . \Statics::$consoleStopped . " Couldn't get ids.txt.\n\n";
            exit;
        }
        preg_match_all('/([\d:]+) # (.+) \((minecraft:\w+)\)/', $idsText, $matches); // https://regex101.com/r/3hmk4K/2
        $extBlockData    = array_map(function (/** @noinspection PhpUnusedParameterInspection */
            string $wholeMatch, string $id, string $label, string $idName) :array {
            return [
                'id'     => $id,
                'label'  => $label,
                'idName' => $idName,
            ];
        }, ...$matches); // https://stackoverflow.com/a/30082922/3088508
        $extBlockDataIds = array_column($extBlockData, 'id');
        return array_combine($extBlockDataIds, $extBlockData);
    }

}
