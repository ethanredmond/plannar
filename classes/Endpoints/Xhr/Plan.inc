<?php
declare(strict_types=1);

namespace Endpoints\Xhr;

class Plan {

    public static function create () :string {
        $fieldErrorJson = \Endpoints\Xhr::validatePostVars('plan');
        if (!empty($fieldErrorJson)) {
            return $fieldErrorJson;
        }
        $planId = \Data\Sql::insert("INSERT INTO plan (objectName, planWidth, planHeight, planData) VALUES (?, ?, ?, ?)", [
            $_POST['objectName'],
            (int) $_POST['planWidth'],
            (int) $_POST['planHeight'],
            $_POST['planData'],
        ]);
        return json_encode(['status' => 'success', 'message' => 'Saved', 'objectHash' => \Func::idToHash($planId)]);
    }

    /**
     * curl --data "objectHash=2" http://www.plannar.com/xhr/plan/update
     * curl --data "objectHash=2&objectName=Test" http://www.plannar.com/xhr/plan/update
     * curl --data "objectHash=2&objectName=Test&planWidth=asdf" http://www.plannar.com/xhr/plan/update
     * curl --data "objectHash=2&objectName=Test&planWidth=50" http://www.plannar.com/xhr/plan/update
     * curl --data "objectHash=2&objectName=Test&planWidth=50&planHeight=asdf" http://www.plannar.com/xhr/plan/update
     * curl --data "objectHash=2&objectName=Test&planWidth=50&planHeight=50" http://www.plannar.com/xhr/plan/update
     * curl --data "objectHash=2&objectName=Test&planWidth=50&planHeight=50&planData=asdf" http://www.plannar.com/xhr/plan/update
     */
    public static function update () :string {
        if (empty($_POST['objectHash'])) {
            return json_encode(['status' => 'error', 'message' => "No plan found :("]);
        }
        $fieldErrorJson = \Endpoints\Xhr::validatePostVars('plan');
        if (!empty($fieldErrorJson)) {
            return $fieldErrorJson;
        }

        $planId = \Func::hashToId($_POST['objectHash']);
        $outArr   = \Data\Sql::select("SELECT objectId, isDeleted FROM plan WHERE objectId = ?", [$planId]);
        if (empty($outArr[0]) || empty($outArr[0]['objectId'])) { // No plan exists.
            return json_encode(['status' => 'error', 'message' => "No plan found :("]);
        } elseif (!empty($outArr[0]['isDeleted'])) { // Is deleted.
            return json_encode(['status' => 'error', 'message' => "This plan's deleted :( <a onclick=\"alert('TODO :P');\">Undelete?</a>"]);
        }

        $paramsArr = [];
        $sql       = "UPDATE plan SET ";
        if (!empty($_POST['objectName'])) {
            $paramsArr[] = $_POST['objectName'];
            $sql         .= 'objectName = ?, ';
        }
        if (!empty($_POST['planWidth'])) {
            $paramsArr[] = $_POST['planWidth'];
            $sql         .= 'planWidth = ?, ';
        }
        if (!empty($_POST['planHeight'])) {
            $paramsArr[] = $_POST['planHeight'];
            $sql         .= 'planHeight = ?, ';
        }
        if (!empty($_POST['planData'])) {
            $paramsArr[] = $_POST['planData'];
            $sql         .= 'planData = ?, ';
        }
        $sql = rtrim($sql, ', ');
        $sql .= ' WHERE objectId = ?';
        if (empty($paramsArr)) {
            return json_encode(['status' => 'error', 'message' => "Nothing to update."]);
        }
        $paramsArr[] = $planId;
        \Data\Sql::update($sql, $paramsArr);
        return json_encode(['status' => 'success', 'message' => 'Saved']);
    }

    public static function delete () :string {
        if (empty($_POST['objectHash'])) {
            return json_encode(['status' => 'error', 'message' => "No plan found :("]);
        }
        $planId = \Func::hashToId($_POST['objectHash']);
        $outArr = \Data\Sql::select("SELECT objectId, isDeleted FROM plan WHERE objectId = ?", [$planId]);
        if (empty($outArr[0]) || empty($outArr[0]['objectId'])) { // No plan exists.
            return json_encode(['status' => 'error', 'message' => "No plan found :("]);
        } elseif (!empty($outArr[0]['isDeleted'])) { // Is already deleted.
            return json_encode(['status' => 'error', 'message' => "This plan is already deleted."]);
        }
        \Data\Sql::update("UPDATE plan SET isDeleted = TRUE WHERE objectId = ?", [$planId]);
        return json_encode(['status' => 'success', 'message' => 'Deleted']);
    }

    public static function undelete () :string {
        if (empty($_POST['objectHash'])) {
            return json_encode(['status' => 'error', 'message' => "No plan found :("]);
        }
        $planId = \Func::hashToId($_POST['objectHash']);
        if (empty($outArr[0]) || empty($outArr[0]['objectId'])) { // No plan exists.
            return json_encode(['status' => 'error', 'message' => "No plan found :("]);
        } elseif (empty($outArr[0]['isDeleted'])) { // Isn't deleted.
            return json_encode(['status' => 'error', 'message' => "This plan isn't deleted."]);
        }
        \Data\Sql::update("UPDATE plan SET isDeleted = FALSE WHERE objectId = ?", [$planId]);
        return json_encode(['status' => 'success', 'message' => 'Undeleted']);
    }

}
