<?php
declare(strict_types=1);

namespace Endpoints;

class Cli {

    /* @var array */
    public static $endpoints = [
        'validateblocks' => '\Endpoints\Cli\Tests::validateBlocks',
    ];

}
