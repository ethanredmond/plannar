<?php
declare(strict_types=1);

namespace Obj;

class PlannarObject {

    /* @var array */
    public static $instancesArr = [];

    /* @var string */
    public static $tableName;
    /* @var array */
    public static $validOpsArr = [];

    /* @var int */
    public $objectId;
    /* @var string */
    public $objectHash;
    /* @var string */
    public $objectType;
    /* @var string */
    public $objectName;
    /* @var string */
    public $objectUrl;

    /* @var bool */
    public $isDeleted;
    /* @var int */
    public $createdUtcUnix;
    /* @var int */
    public $lastUpdatedUtcUnix;

    protected static function getFromId (int $objectId) :?\Obj\PlannarObject {
        if (!empty(self::$instancesArr[static::$tableName]) && array_key_exists($objectId, self::$instancesArr[static::$tableName])) {
            return self::$instancesArr[static::$tableName][$objectId];
        }
        $foundObj                                          = new static(\Data\Sql::select("SELECT * FROM " . static::$tableName . " WHERE objectId = ?", [$objectId])[0] ?? []);
        self::$instancesArr[static::$tableName][$objectId] = $foundObj;
        return $foundObj;
    }

    public static function getArray (array $objectIdsArr) :array {
        $objectType           = lcfirst(str_replace("Obj\\", '', get_called_class()));
        $objsArr              = [];
        $objectIdsToSelectArr = [];
        foreach ($objectIdsArr as $objectId) {
            if (!empty(self::$instancesArr[$objectType]) && array_key_exists($objectId, self::$instancesArr[$objectType])) {
                $objsArr[$objectId] = self::$instancesArr[$objectId];
            } else {
                $objectIdsToSelectArr[] = $objectId;
            }
        }
        if ($objectIdsToSelectArr) {
            $foundObjsArr = \Data\Sql::select("SELECT * FROM " . static::$tableName . " WHERE objectId IN (" . str_repeat('?, ', count($objectIdsToSelectArr) - 1) . '?' . ")", $objectIdsToSelectArr);
            foreach ($foundObjsArr as $foundArr) {
                $foundObj                                  = new static($foundArr);
                self::$instancesArr[$foundArr['objectId']] = $foundObj;
                $objsArr[$foundArr['objectId']]            = $foundObj;
            }
        }
        return $objsArr;
    }

    public function __construct (array $propertiesArr) {
        if (!empty($propertiesArr)) {
            foreach ($propertiesArr as $fieldName => $fieldValue) {
                $fieldName        = str_replace('Ymd', 'Unix', str_replace('YmdHms', 'Unix', $fieldName));
                $this->$fieldName = $fieldValue;
                if (\Func::endsWith($fieldName, 'Data')) {
                    $this->$fieldName = json_decode($this->$fieldName, true);
                    if (!empty(json_last_error())) {
                        \Endpoints\Page\Msg::html([
                            'msgHeading' => "Error",
                            'msgText'    => json_last_error_msg(),
                        ]);
                        exit;
                    }
                }
            }
        }
        if (!empty($this->objectId)) {
            $this->objectHash = \Func::idToHash($this->objectId);
        }
        $this->objectUrl = "/$this->objectType:$this->objectHash/";
    }

    /**
     * An empty object still has all properties defined, but objectId is null
     */
    public function isEmpty () :bool {
        return (empty($this->objectId));
    }

}
