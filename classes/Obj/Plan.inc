<?php
declare(strict_types=1);

namespace Obj;

class Plan extends \Obj\PlannarObject {

    /* @var string */
    public static $tableName = 'plan';
    /* @var string */
    public $objectType = 'plan';
    /* @var array */
    public static $validOpsArr = ['select', 'update', 'delete', 'restore', 'view', 'like'];

    /* @var int */
    public $planWidth;
    /* @var int */
    public $planHeight;
    /* @var array */
    public $planData;
    /* @var int */
    public $viewsNum;
    /* @var int */
    public $likesNum;
    /* @var int */
    public $userId;

    public static function get (?int $objectId) :self {
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return parent::getFromId($objectId);
    }

}
