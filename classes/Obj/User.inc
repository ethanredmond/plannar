<?php
declare(strict_types=1);

namespace Obj;

class User extends \Obj\PlannarObject {

    /* @var string */
    public static $tableName = 'user';
    /* @var string */
    public $objectType = 'user';
    /* @var array */
    public static $validOpsArr = ['select', 'update', 'delete', 'restore', 'view'];

    /* @var string */
    public $loginEmailAddress;
    /* @var string */
    public $bioText;
    /* @var bool */
    public $isAdmin;
    /* @var int */
    public $googleId;
    /* @var string */
    public $session_str;
    /* @var int */
    public $session_startUtcUnix;
    /* @var int */
    public $session_endUtcUnix;

    public static function get (?int $objectId) :self {
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return parent::getFromId($objectId);
    }

}
