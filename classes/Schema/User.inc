<?php
declare(strict_types=1);

namespace Schema;

class User {

    /* @var array */
    public static $objectFieldsArr = [
        // @formatter:off
        'objectId'               => ['isUpdated' => false, 'isRequired' => true,  'dataType' => 'i'],
        'objectName'             => ['isUpdated' => true,  'isRequired' => false, 'dataType' => 's70'],
        'loginEmailAddress'      => ['isUpdated' => false, 'isRequired' => true,  'dataType' => 'e'],
        'bioText'                => ['isUpdated' => true,  'isRequired' => false, 'dataType' => 's1000'],
        'isAdmin'                => ['isUpdated' => true,  'isRequired' => true,  'dataType' => 'b'],
        'googleId'               => ['isUpdated' => false, 'isRequired' => true,  'dataType' => 'Y'],
        'session_str'            => ['isUpdated' => false, 'isRequired' => true,  'dataType' => 'i'],
        'session_startUtcYmdHms' => ['isUpdated' => false, 'isRequired' => true,  'dataType' => 't'],
        'session_endUtcYmdHms'   => ['isUpdated' => false, 'isRequired' => true,  'dataType' => 't'],
        'createdUtcYmdHms'       => ['isUpdated' => false, 'isRequired' => true,  'dataType' => 't'],
        'lastUpdatedUtcYmdHms'   => ['isUpdated' => false, 'isRequired' => true,  'dataType' => 't'],
        'isDeleted'              => ['isUpdated' => false, 'isRequired' => true,  'dataType' => 'b'],
        // @formatter:on
    ];

}
