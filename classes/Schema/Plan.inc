<?php
declare(strict_types=1);

namespace Schema;

class Plan {

    /* @var array */
    public static $objectFieldsArr = [
        // @formatter:off
        'objectId'              => ['isUpdated' => false, 'isRequired' => true,  'dataType' => 'i'],
        'objectName'            => ['isUpdated' => true,  'isRequired' => true,  'dataType' => 's50'],
        'planWidth'             => ['isUpdated' => true,  'isRequired' => true,  'dataType' => 'Y'],
        'planHeight'            => ['isUpdated' => true,  'isRequired' => true,  'dataType' => 'Y'],
        'planData'              => ['isUpdated' => true,  'isRequired' => true,  'dataType' => 'x'],
        'viewsNum'              => ['isUpdated' => false, 'isRequired' => false, 'dataType' => 'Y'],
        'likesNum'              => ['isUpdated' => false, 'isRequired' => false, 'dataType' => 'Y'],
        'userId'                => ['isUpdated' => false, 'isRequired' => true,  'dataType' => 'i'],
        'createdUtcYmdHms'      => ['isUpdated' => false, 'isRequired' => true,  'dataType' => 't'],
        'lastUpdatedUtcYmdHms'  => ['isUpdated' => false, 'isRequired' => true,  'dataType' => 't'],
        'isDeleted'             => ['isUpdated' => false, 'isRequired' => true,  'dataType' => 'b'],
        // @formatter:on
    ];

}
