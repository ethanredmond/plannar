<?php
declare(strict_types=1);

class Statics {

    /* @var array */
    public static $backgroundsArr = [
        'https://b.thumbs.redditmedia.com/lTE90J4ht-6vP0OU.png',
        'https://c.thumbs.redditmedia.com/i-gpWTu8cJ6pV57V.png',
        'https://d.thumbs.redditmedia.com/LS4gnrviSZ98q3_u.png',
        'https://e.thumbs.redditmedia.com/N-pjloe-auKo_GSL.png',
        'https://c.thumbs.redditmedia.com/7NxhvJURQXqOTAgn.png',
        'https://f.thumbs.redditmedia.com/Je8ZX0fVqt2gF-XA.png',
        'https://b.thumbs.redditmedia.com/56RRD5jEx-fRQBxa.png',
        'https://f.thumbs.redditmedia.com/s4cZDmp_C7FG8_45.png',
        'https://e.thumbs.redditmedia.com/ZGIrVolYnJqcmb5v.png',
        'https://b.thumbs.redditmedia.com/7WfE4hqW0RRu-K-I.png',
        'https://b.thumbs.redditmedia.com/GNSRsrl0aExERk7a.png',
        'https://c.thumbs.redditmedia.com/cYSpOg0lC1YikXMb.png',
        'https://a.thumbs.redditmedia.com/Gtp23tD0GGNKR8Qx.png',
    ];
    /* @var string */
    public static $redStart = "\033[41;1m";
    /* @var string */
    public static $redStop = "\033[0m";
    /* @var string */
    public static $greenStart = "\033[42;1m";
    /* @var string */
    public static $greenStop = "\033[0m";
    /* @var string */
    public static $magentaStart = "\x1b[45m";
    /* @var string */
    public static $magentaStop = "\033[0m";
    /* @var string */
    public static $blueStart = "\x1b[44m";
    /* @var string */
    public static $blueStop = "\033[0m";
    /* @var string */
    public static $consoleStopped;
    /* @var string */
    public static $consoleFailed;
    /* @var string */
    public static $consolePassed;
    /* @var string */
    public static $consoleWarning;
    /* @var string */
    public static $consoleFinished;
    /* @var bool */
    public static $isCli;
    /* @var bool */
    public static $isAjax;
    /* @var string */
    public static $selectableEventHandlersAttr;
    /* @var string */
    public static $selectableCodeAttr;
    /* @var int */
    public static $secs_min = 60;
    /* @var int */
    public static $secs_hr = 60 * 60;
    /* @var int */
    public static $secs_day = 60 * 60 * 24;
    /* @var int */
    public static $secs_wk = 60 * 60 * 24 * 7;
    /* @var int */
    public static $secs_mth = 2629742; // From: round(60 * 60 * 24 * 365.242 / 12).
    /* @var int */
    public static $secs_yr = 31556909; // From: round(60 * 60 * 24 * 365.242).
    /* @var bool */
    public static $isDev = false;

    public static function init () :void {
        // @formatter:off
        self::$consoleStopped  = self::$redStart        . "  STOPPED  "  . self::$redStop;
        self::$consoleFailed   = self::$redStart        . "  FAILED  "   . self::$redStop;
        self::$consolePassed   = self::$greenStart      . "  PASSED  "   . self::$greenStop;
        self::$consoleWarning  = self::$magentaStart    . "  WARNING  "  . self::$magentaStop;
        self::$consoleFinished = self::$blueStart       . "  FINISHED  " . self::$blueStop;
        // @formatter:on
        self::$isCli                       = (empty($_SERVER['HTTP_HOST']) && empty($_SERVER['HTTP_X_SERVER_ADDRESS']) && empty($_SERVER['SERVER_ADDR']));
        self::$isAjax                      = (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
        self::$selectableEventHandlersAttr = "onmouseout=\"doDeSelect();\" onmouseover=\"if (doSelectMouseEvent) { doSelectMouseEvent(event); } else { top.doSelectMouseEvent(event); }\"";
        self::$selectableCodeAttr          = "class=\"selectable\" " . self::$selectableEventHandlersAttr;
    }

}

\Statics::init();
