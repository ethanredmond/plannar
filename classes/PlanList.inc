<?php
declare(strict_types=1);

class PlanList {

    public static function html (array $PLAN_arr) :string {
        $out = '';
        $out .= "  <div class=\"row planList\">\n";
        $out .= "   <div class=\"col m4\">\n";
        $out .= "    <a class=\"card planCard addNewCard waves-effect\" href=\"/new-plan\">\n";
        $out .= "     <div class=\"card-content\">\n";
        $out .= "      <i class=\"material-icons medium\">add</i>\n";
        $out .= "      <br>\n";
        $out .= "      Add new\n";
        $out .= "     </div>\n";
        $out .= "    </a>\n";
        $out .= "   </div>\n";
        /* @var \Obj\Plan[] $PLAN_arr */
        for ($i = 0; $i < 20; $i++) {
            foreach ($PLAN_arr as $PLAN) {
                $out .= "   <div class=\"col m4\">\n";
                $out .= "    <div class=\"card planCard waves-effect\">\n";
                $out .= "     <a class=\"card-image\" href=\"$PLAN->objectUrl\">\n";
                $out .= "      <img src=\"https://picsum.photos/g/500/400/\">\n";
                $out .= "     </a>\n";
                $out .= "     <div class=\"card-content\">\n";
                $out .= "      <span class=\"card-title\">\n";
                $out .= "       <a style=\"cursor: pointer;\" href=\"$PLAN->objectUrl\">" . htmlspecialchars($PLAN->objectName) . "</a>\n";
                $out .= "       <span class=\"badge statBadge\" data-badge-caption=\"" . \Func::formatNum($PLAN->viewsNum) . "\"><i class=\"tiny material-icons\">visibility</i></span>\n";
                $out .= "       <span class=\"badge statBadge\" data-badge-caption=\"" . \Func::formatNum($PLAN->likesNum) . "\"><i class=\"tiny material-icons\">thumb_up</i></span>\n";
                $out .= "      </span>\n";
                $out .= "      <p class=\"truncate\">\n";
                $out .= "       By <a href=\"$PLAN->objectUrl\" target=\"_blank\">" . htmlspecialchars((string) $PLAN->userId) . "</a>.<br>\n";
                $out .= "      </p>\n";
                $out .= "      <p class=\"truncate\">\n";
                // $PLAN->lastUpdatedUtcUnix
                $out .= "       Updated 5 mins ago.<br>\n";
                $out .= "      </p>\n";
                $out .= "     </div>\n";
                $out .= "    </div>\n";
                $out .= "   </div>\n";
            }
        }
        $out .= "  </div>\n";
        return $out;
    }

}
