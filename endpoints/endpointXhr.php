<?php
declare(strict_types=1);

// Use url rewriting like: https://www.plannar.com/xhr/endpoint?qsvar=1.

if (!empty($_GET['endpoint']) && array_key_exists($_GET['endpoint'], \Endpoints\Xhr::$endpoints)) {
    $endpointClassName = \Endpoints\Xhr::$endpoints[$_GET['endpoint']];
    echo $endpointClassName();
} else {
    header('HTTP/1.0 404 Not Found');
    exit;
}
