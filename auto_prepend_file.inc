<?php
/** @noinspection PhpIncludeInspection */
declare(strict_types=1);

spl_autoload_register(function (string $className) :void {
    $className = str_replace('\\', '/', $className);
    if (file_exists("/mnt/ebs1/git/plannar/classes/$className.inc")) {
        require "/mnt/ebs1/git/plannar/classes/$className.inc";
        return;
    }
});

require_once '/mnt/ebs1/git/plannar/vendor/autoload.php';
