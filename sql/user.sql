CREATE TABLE plannar.user (
  objectId                                      int UNSIGNED       NOT NULL AUTO_INCREMENT,
  objectName                                    varchar(70)        NULL,
  loginEmailAddress                             varchar(254)       NOT NULL,
  bioText                                       varchar(1000)      NULL,
  isAdmin                                       bool               NOT NULL DEFAULT FALSE,
  googleId                                      varchar(21)        NOT NULL,
  session_str                                   varchar(128)       NOT NULL,
  session_startUtcYmdHms                        datetime           NOT NULL,
  session_endUtcYmdHms                          datetime           NOT NULL,
  createdUtcYmdHms                              datetime           NOT NULL DEFAULT CURRENT_TIMESTAMP,
  lastUpdatedUtcYmdHms                          datetime           NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  isDeleted                                     bool               NOT NULL DEFAULT FALSE, -- TRUE = deleted, FALSE = not deleted
  PRIMARY KEY (
    objectId
  )
) ENGINE = InnoDB;
ALTER TABLE plannar.user ADD UNIQUE INDEX (
  objectId
);
ALTER TABLE plannar.user ADD UNIQUE INDEX (
  googleId
);
