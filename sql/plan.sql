CREATE TABLE plannar.plan (
  objectId                                      int UNSIGNED       NOT NULL AUTO_INCREMENT,
  objectName                                    varchar(50)        NOT NULL,
  planWidth                                     smallint UNSIGNED  NOT NULL,
  planHeight                                    smallint UNSIGNED  NOT NULL,
  planData                                      text               NOT NULL,
  viewsNum                                      smallint UNSIGNED  NOT NULL DEFAULT 0,
  likesNum                                      smallint UNSIGNED  NOT NULL DEFAULT 0,
  userId                                        int UNSIGNED       NULL,
  createdUtcYmdHms                              datetime           NOT NULL DEFAULT CURRENT_TIMESTAMP,
  lastUpdatedUtcYmdHms                          datetime           NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  isDeleted                                     bool               NOT NULL DEFAULT FALSE, -- TRUE = deleted, FALSE = not deleted
  PRIMARY KEY (
    objectId
  )
) ENGINE = InnoDB;
ALTER TABLE plannar.plan ADD UNIQUE INDEX (
  objectId
);
